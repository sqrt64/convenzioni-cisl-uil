# -*- coding: utf-8 -*-
from django import forms
from django.contrib.sites.shortcuts import get_current_site

from captcha.fields import CaptchaField

from agreements.models import Region, Province, City, Category


class SuggestionsForm(forms.Form):
    email = forms.EmailField(label='e-mail', required=True)
    message = forms.CharField(
        label='messaggio',
        required=True,
        widget=forms.Textarea)
    captcha = CaptchaField()


class ProposalForm(forms.Form):
    firstname = forms.CharField(label='nome', required=True)
    lastname = forms.CharField(label='cognome', required=True)
    phone = forms.CharField(label='telefono', required=True)
    head_office = forms.CharField(label='sede sindacale', required=True)
    referer = forms.CharField(
        label='nominativo del referente/titolare punto vendita',
        required=True,
        help_text='con cui si è stipulata la convenzione')
    begin_date = forms.CharField(label='data stipula convenzione',
                                 required=True)
    end_date = forms.CharField(label='data termine convenzione',
                               required=False)

    company_name = forms.CharField(label='nome azienda', required=True)
    company_address = forms.CharField(label='indirizzo', required=True)
    company_region = forms.ModelChoiceField(
        label='regione',
        required=False,
        queryset=Region.objects.all())
    company_region_other = forms.CharField(label='altra regione',
                                           required=False)
    company_province = forms.ModelChoiceField(
        label='provincia',
        empty_label='clicca qui per scegliere',
        required=False,
        queryset=Province.objects.all())
    company_province_other = forms.CharField(label='altra provincia',
                                             required=False)
    company_city = forms.ModelChoiceField(
        label='comune',
        empty_label='clicca qui per scegliere',
        required=False,
        queryset=City.objects.all())
    company_city_other = forms.CharField(label='altro comune',
                                         required=False)
    company_phone = forms.CharField(label='telefono', required=True)
    company_web = forms.CharField(label='sito web', required=False)
    company_email = forms.CharField(label='e-mail', required=False)
    company_category = forms.ModelChoiceField(
        label='categoria merceologica',
        empty_label='clicca qui per scegliere',
        required=False,
        queryset=Category.objects.filter(sites__in=[get_current_site(None), ]))
    # queryset=Category.objects.all())
    company_category_other = forms.CharField(label='nuova categoria',
                                             required=False)
    company_category_other2 = forms.CharField(label='altra categoria',
                                             required=False)
    company_description = forms.CharField(
        label='descrizione, percentuale sconto/agevolazione',
        required=True,
        widget=forms.Textarea)
    captcha = CaptchaField()
    site = forms.IntegerField(label='sito', required=False)
