"""URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from rest_framework import routers

from .views import SuggestionsFormView, RewriteView, AgreementProposalView, news_feeds # noqa
from agreements.views import CategoryViewSet, CompanyViewSet, PointViewSet
from ads.views import BannerViewSet

# API
router = routers.DefaultRouter()
router.register(r'categorie', CategoryViewSet)
router.register(r'aziende', CompanyViewSet)
router.register(r'rivenditori', PointViewSet)
router.register(r'ads', BannerViewSet)

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
    # login
    url(r'^login/?$', 'django.contrib.auth.views.login'),
    url(r'^logout/?$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    # ckeditor uploader
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    # treenav
    url(r'^treenav/', include('treenav.urls')),
    # ads
    url(r'^ads/', include('ads.urls')),
    # news
    url(r'^news/', include('news.urls')),
    # suggerimenti
    url(r'^suggerimenti/inviati/', TemplateView.as_view(template_name='core/suggestions_sent.html'), name="suggestions"), # noqa
    url(r'^suggerimenti/', SuggestionsFormView.as_view(), name="suggestions"), # noqa
    # agreements
    url(r'^convenzioni/', include('agreements.urls')),
    # smart_selects
    url(r'^chaining/', include('smart_selects.urls')),
    # captcha
    url(r'^captcha/', include('captcha.urls')),
    # agreements
    url(r'inserisci-convenzioni/inviata/', TemplateView.as_view(template_name='core/proposal_sent.html'), name="proposals"), # noqa
    url(r'^inserisci-convenzioni/', login_required(AgreementProposalView.as_view())),
    # old urls
    url(r'^r/', RewriteView.as_view(), name="rewrite"), # noqa
    url(r'^robots\.txt$', TemplateView.as_view(template_name='core/robots.txt', content_type='text/plain')), # noqa
    # newsfeeds
    url(r'^newsfeeds/', news_feeds),
    # blog
    url(r'^blog/', include('blog.urls')),
    # API
    url(r'^api/v1/', include(router.urls))
]

if settings.DEBUG:
    urlpatterns += [
        url(r'^media/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT}),
    ]
    urlpatterns += [
        url(r'^static/(?P<path>.*)$',
            'django.contrib.staticfiles.views.serve'),
    ]

    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
