import json
import feedparser
from dateutil import parser

from django.views.generic.edit import FormView
from django.views.generic import View
from django.shortcuts import get_object_or_404, redirect, render
from django.core.mail import send_mail
from django.conf import settings
from django.http import Http404
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.sites.models import Site

from .forms import SuggestionsForm, ProposalForm
from agreements.models import Category, Region, Province, Proposal


class SuggestionsFormView(FormView):
    template_name = 'core/suggestions.html'
    form_class = SuggestionsForm
    success_url = '/suggerimenti/inviati/'

    def form_valid(self, form):

        email = form.cleaned_data['email']
        message = form.cleaned_data['message']

        if email and message:
            mail_subject = '%s - Suggerimenti da %s' % (self.request.site.name, email) # noqa
            mail_object = message
            send_mail(mail_subject, mail_object, settings.SITE['MAIL_NOREPLY'], (settings.SITE['MAIL_CONVENZIONI'], )) # noqa

        return super(SuggestionsFormView, self).form_valid(form)


class RewriteView(View):

    def get(self, request):
        if (request.GET.get('idcategoria', None)):
            category = get_object_or_404(Category, old_id=request.GET.get('idcategoria')) # noqa
            return redirect('agreements-list', category_slug=category.slug)
        if (request.GET.get('IDCATEGORIA', None)):
            category = get_object_or_404(Category, old_id=request.GET.get('IDCATEGORIA')) # noqa
            return redirect('agreements-list', category_slug=category.slug)
        raise Http404


class AgreementProposalView(FormView):
    template_name = 'core/proposal.html'
    form_class = ProposalForm
    success_url = '/inserisci-convenzioni/inviata/'

    def get_context_data(self, **kwargs):
        context = super(AgreementProposalView, self).get_context_data(**kwargs)
        current_site = get_current_site(None)
        context['site'] = current_site.id
        regions = {}
        provinces = {}
        for r in Region.objects.all():
            arr = []
            for p in r.provinces.all():
                arr.append(p.id)
            regions[r.id] = arr
        context['regions_dict'] = json.dumps(regions)
        for p in Province.objects.all():
            arr = []
            for c in p.cities.all():
                arr.append(c.id)
            provinces[p.id] = arr
        context['provinces_dict'] = json.dumps(provinces)
        return context

    def form_valid(self, form):
        firstname = form.cleaned_data['firstname']
        lastname = form.cleaned_data['lastname']
        phone = form.cleaned_data['phone']
        head_office = form.cleaned_data['head_office']
        referer = form.cleaned_data['referer']
        begin_date = form.cleaned_data['begin_date']
        end_date = form.cleaned_data['end_date']
        company_name = form.cleaned_data['company_name']
        company_address = form.cleaned_data['company_address']
        company_region = form.cleaned_data['company_region']
        company_region_other = form.cleaned_data['company_region_other']
        company_province = form.cleaned_data['company_province']
        company_province_other = form.cleaned_data['company_province_other']
        company_city = form.cleaned_data['company_city']
        company_city_other = form.cleaned_data['company_city_other']
        company_phone = form.cleaned_data['company_phone']
        company_web = form.cleaned_data['company_web']
        company_email = form.cleaned_data['company_email']
        company_category = form.cleaned_data['company_category']
        company_category_other = form.cleaned_data['company_category_other']
        company_category_other2 = form.cleaned_data['company_category_other2']
        company_description = form.cleaned_data['company_description']
        site = form.cleaned_data['site']

        proposal = Proposal(
            firstname=firstname,
            lastname=lastname,
            phone=phone,
            head_office=head_office,
            referer=referer,
            begin_date=begin_date,
            end_date=end_date,
            company_name=company_name,
            company_address=company_address,
            company_region=company_region,
            company_region_other=company_region_other,
            company_province=company_province,
            company_province_other=company_province_other,
            company_city=company_city,
            company_city_other=company_city_other,
            company_phone=company_phone,
            company_web=company_web,
            company_email=company_email,
            company_category=company_category,
            company_category_other=company_category_other,
            company_category_other2=company_category_other2,
            company_description=company_description,
            site=Site.objects.get(id=site),
        )
        proposal.save()

        current_site = get_current_site(None)

        send_mail(
            'Inserimento nuova convenzione',
            'Nuova proposta di convenzione inserita su %s.' % current_site.domain, # noqa
            settings.SITE['MAIL_NOREPLY'],
            ('direzione@rtpcomunicazione.it', )
        ) # noqa

        return super(AgreementProposalView, self).form_valid(form)


def news_feeds(request):
    current_site = get_current_site(request)
    url = settings.SITE['FEED_URL']
    feeds = feedparser.parse(url)
    fs = []
    try:
        for i in range(0, 6):
            feed = feeds['entries'][i]
            obj = {
                'feed': feed,
                'published': parser.parse(feed.published).strftime('%d %b %Y'),
                'link': feed.links[0]['href']
            }
            fs.append(obj)
    except:
        fs = []

    return render(
        request,
        'core/news_feeds_content.html',
        {
            'site': current_site,
            'feeds': feeds,
            'fs': fs,
            'list_url': settings.SITE['FEED_PAGE'],
            'union': settings.SITE['UNION']
        }
    )
