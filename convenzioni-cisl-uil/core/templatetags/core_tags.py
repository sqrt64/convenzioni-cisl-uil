import feedparser
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

from dateutil import parser

from django import template
from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings


register = template.Library()


@register.inclusion_tag('core/newsletter_registration_form_cisl.html',
                        takes_context=True)
def newsletter_registration_form_cisl(context):
    return {}


@register.inclusion_tag('core/newsletter_registration_form_uil.html',
                        takes_context=True)
def newsletter_registration_form_uil(context):
    return {}


@register.inclusion_tag('core/newsletter_registration_form_uilmarche.html',
                        takes_context=True)
def newsletter_registration_form_uilmarche(context):
    return {}


@register.inclusion_tag('core/news_feeds.html',
                        takes_context=True)
def news_feeds(context):

    return {
        'union': settings.SITE['UNION']
    }


@register.filter()
def strip_img(html):
    import re
    TAG_RE = re.compile(r'<img.+>')
    return TAG_RE.sub('', html)


@register.filter()
def abs_url(url):
    current_site = get_current_site(None)
    return ''.join([
        'http://',
        current_site.domain,
        url
    ])
