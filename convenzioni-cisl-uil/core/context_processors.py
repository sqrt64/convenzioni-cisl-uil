from django.conf import settings


def debug(context):
    return {'DEBUG': settings.DEBUG}


def site(context):
    return {'SITE': settings.SITE}


def maps(context):
    return {'MAPS_API_KEY': settings.MAPS_API_KEY}
