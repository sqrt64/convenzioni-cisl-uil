'''This module sets the configuration for a local development

'''
from .common_uilmarche import *

import os

DEBUG = False

ALLOWED_HOSTS = ['convenzioniuilmarche.sqrt64.it', 'convenzioniuil.marche.it', 'www.convenzioniuil.marche.it', ]

SESSION_COOKIE_SECURE=True
SESSION_COOKIE_HTTPONLY=True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')
SESSION_COOKIE_SECURE = True

STATIC_ROOT = '/home/otto/www/convenzioni-cisl-uil/static/'
MEDIA_ROOT = '/home/otto/www/convenzioni-cisl-uil/media'
# CKEDITOR
CKEDITOR_CONFIGS['default']['contentsCss'] = [
    STATIC_URL + 'core/css/vendor.min.css',
    STATIC_URL + 'core/css/core.uil.min.css',
    STATIC_URL + 'core/src/css/ckeditor.css']

LOGGING['handlers']['file']['filename'] = here('..', '..', '..', '..', os.path.join('logs', 'debug.log'))
