# -*- coding: utf-8 -*-
'''This module sets the configuration for a local development

'''
from .common import * # noqa
from dotenv import load_dotenv
from getenv import env

dotenv_path = here('..', '..', '.env')
load_dotenv(dotenv_path)

# SITE
SITE_ID = 3

# DISQUS
DISQUS_API_KEY = env('DISQUS_API_KEY_UIL', '')
DISQUS_WEBSITE_SHORTNAME = 'convenzioniuilmarche'

# OTTO ADMIN
OA_ANALYTICS_VIEW_ID = '139346285'

# SITE PROPS
SITE = {
    'SITE': 'UILMARCHE',
    'UNION': 'UIL MARCHE',
    'TITLE': 'Convenzioni UIL Marche',
    'DESCRIPTION': 'su convenzioniuil.marche.it trovi tutte le convenzioni attive per i tesserati UIL e i loro familiari, per risparmiare tutto l\'anno.', # noqa
    'KEYWORDS': 'convenzioni,uil,sconto uil, abbigliamento, badante, apparecchi acustici, articoli sportivi, assicurazioni,automobili, concessionaria auto, autoriparazioni, centro medico, casa di riposo, residenza per anziani,finanziamento, prestito, gioielli, pneumatici, arredamento, traslochi, viaggi,turismo, dentisti, odontoiatria,vini, ortopedia, ottico, onoranze funebri, salute, ristoranti, pizzeria,hotel', # noqa
    'URL': 'http://convenzioniuil.marche.it',
    'LOGO': '/static/core/uilmarche/img/logo.jpg',
    'MENU': 'menu-uilmarche',
    'FACEBOOK_URL': 'https://www.facebook.com/UILsindacatomarche/',
    'MAIL_NOREPLY': 'no-reply@convenzioniuil.marche.it',
    'MAIL_INFO': 'info@convenzioniuil.marche.it',
    'MAIL_MARKETING': 'marketing@convenzioniuil.marche.it',
    'MAIL_CONVENZIONI': 'convenzioni@convenzioniuil.it',
    'FEED_URL': 'https://www.uil-marche.it/feed/',
    'FEED_PAGE': 'https://www.uil-marche.it',
    'PLAY_STORE_URL': '', # noqa
    'ITUNES_STORE_URL': '', # noqa
    'GA': 'G-5VYHDJCW56',
    'COOKIE_RULEZ': 'https://cookierulez.sqrt64.it/widget/P2qgD1nT3SVmoJM3GwBUfqs563N96DLg',
    'NEWSLETTER_SUBSCRIBE_PAGE': 'https://myt50.mailrouter.it/user/register',
    'TAWK_KEY': 'https://embed.tawk.to/5c616f9e7cf662208c950130/default',
}
