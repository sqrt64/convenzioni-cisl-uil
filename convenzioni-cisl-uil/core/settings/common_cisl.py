# -*- coding: utf-8 -*-
'''This module sets the configuration for a local development

'''
from .common import * # noqa
from dotenv import load_dotenv
from getenv import env

dotenv_path = here('..', '..', '.env')
load_dotenv(dotenv_path)

# SITE
SITE_ID = 1

# DISQUS
DISQUS_API_KEY = env('DISQUS_API_KEY_CISL', '')
DISQUS_WEBSITE_SHORTNAME = 'convenzionicisl'

# OTTO ADMIN
OA_ANALYTICS_VIEW_ID = '139317485'

# SITE PROPS
SITE = {
    'SITE': 'CISL',
    'UNION': 'CISL',
    'TITLE': 'Convenzioni CISL',
    'DESCRIPTION': 'su convenzionicisl.it trovi tutte le convenzioni attive per i tesserati CISL e i loro familiari, per risparmiare tutto l\'anno.', # noqa
    'KEYWORDS': 'convenzioni,cisl,sconto cisl, abbigliamento, badante, apparecchi acustici, articoli sportivi, assicurazioni,automobili, concessionaria auto, autoriparazioni, centro medico, casa di riposo, residenza per anziani,finanziamento, prestito, gioielli, pneumatici, arredamento, traslochi, viaggi,turismo, dentisti, odontoiatria,vini, ortopedia, ottico, onoranze funebri, salute, ristoranti, pizzeria,hotel', # noqa
    'URL': 'http://www.convenzionicisl.it',
    'LOGO': '/static/core/cisl/img/logo.jpg',
    'MENU': 'menu-cisl',
    'FACEBOOK_URL': 'https://www.facebook.com/convenzionicisl',
    'MAIL_NOREPLY': 'no-reply@convenzionicisl.it',
    'MAIL_INFO': 'info@convenzionicisl.it',
    'MAIL_MARKETING': 'marketing@convenzionicisl.it',
    'MAIL_CONVENZIONI': 'convenzioni@convenzionicisl.it',
    'FEED_URL': 'http://www.cislpiemonte.it/tutte-le-news/feed/',
    'FEED_PAGE': 'http://www.cislpiemonte.it/cislpiemonte/tutte-le-news/',
    'PLAY_STORE_URL': 'https://play.google.com/store/apps/details?id=it.softwaresystem.convenzionicisl&feature=search_result#?t=W251bGwsMSwyLDEsIml0LnNvZnR3YXJlc3lzdGVtLmNvbnZlbnppb25pY2lzbCJd', # noqa
    'ITUNES_STORE_URL': 'https://itunes.apple.com/it/app/convenzioni-cisl/id640052719?mt=8', # noqa
    'GA': 'UA-64970577-2',
    'COOKIE_RULEZ': 'https://cookierulez.sqrt64.it/widget/pl7M4zvA1UtSFZKXin00MiJDZiBFtmDm',
    'NEWSLETTER_SUBSCRIBE_PAGE': 'https://mc1l3.mailrouter.it/user/register',
    'TAWK_KEY': 'https://embed.tawk.to/5c52fb8051410568a1097f09/1d2i370ui',
}
