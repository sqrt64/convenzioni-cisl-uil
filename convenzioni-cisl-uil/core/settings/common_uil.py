# -*- coding: utf-8 -*-
'''This module sets the configuration for a local development

'''
from .common import * # noqa
from dotenv import load_dotenv
from getenv import env

dotenv_path = here('..', '..', '.env')
load_dotenv(dotenv_path)

# SITE
SITE_ID = 2

# DISQUS
DISQUS_API_KEY = env('DISQUS_API_KEY_UIL', '')
DISQUS_WEBSITE_SHORTNAME = 'convenzioniuil'

# OTTO ADMIN
OA_ANALYTICS_VIEW_ID = '139346285'

# SITE PROPS
SITE = {
    'SITE': 'UIL',
    'UNION': 'UIL',
    'TITLE': 'Convenzioni UIL',
    'DESCRIPTION': 'su convenzioniuil.it trovi tutte le convenzioni attive per i tesserati UIL e i loro familiari, per risparmiare tutto l\'anno.', # noqa
    'KEYWORDS': 'convenzioni,uil,sconto uil, abbigliamento, badante, apparecchi acustici, articoli sportivi, assicurazioni,automobili, concessionaria auto, autoriparazioni, centro medico, casa di riposo, residenza per anziani,finanziamento, prestito, gioielli, pneumatici, arredamento, traslochi, viaggi,turismo, dentisti, odontoiatria,vini, ortopedia, ottico, onoranze funebri, salute, ristoranti, pizzeria,hotel', # noqa
    'URL': 'http://www.convenzioniuil.it',
    'LOGO': '/static/core/uil/img/logo.jpg',
    'MENU': 'menu-uil',
    'FACEBOOK_URL': 'https://www.facebook.com/convenzioniuil',
    'MAIL_NOREPLY': 'no-reply@convenzioniuil.it',
    'MAIL_INFO': 'info@convenzioniuil.it',
    'MAIL_MARKETING': 'marketing@convenzioniuil.it',
    'MAIL_CONVENZIONI': 'convenzioni@convenzioniuil.it',
    'FEED_URL': 'http://www.uil.it/documents/rss-feed.xml',
    'FEED_PAGE': 'http://www.uilpiemonte.it',
    'PLAY_STORE_URL': 'https://play.google.com/store/apps/details?id=it.softwaresystem.convenzioniuil', # noqa
    'ITUNES_STORE_URL': 'https://itunes.apple.com/it/app/convenzioni-uil/id741995329?mt=8', # noqa
    'GA': 'UA-64970577-3',
    'COOKIE_RULEZ': 'https://cookierulez.sqrt64.it/widget/P2qgD1nT3SVmoJM3GwBUfqs563N96DLg',
    'NEWSLETTER_SUBSCRIBE_PAGE': 'https://myt50.mailrouter.it/user/register',
    'TAWK_KEY': 'https://embed.tawk.to/5c616f9e7cf662208c950130/default',
}
