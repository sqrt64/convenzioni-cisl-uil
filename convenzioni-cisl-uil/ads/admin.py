import pygeoip

from django.contrib import admin
from django.utils import timezone
from django.utils.html import mark_safe

from .models import Banner, Schedule, Click


class ScheduleInline(admin.TabularInline):
    '''
    Tabular Inline View for Schedule
    '''
    model = Schedule
    extra = 1
    suit_classes = 'suit-tab suit-tab-schedule'


class BannerAdmin(admin.ModelAdmin):
    list_display = ('client', 'url', 'relevance', 'position', 'show_in_app',
                    'show_in_site', 'get_sites', 'get_status', )
    list_filter = ('relevance', 'show_in_app', 'show_in_site', )
    list_editable = ('relevance', 'position', )
    inlines = [ScheduleInline]
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['client', 'url', 'image', 'app_image', 'relevance',
                       'position', 'show_in_site', 'show_in_app', 'sites', ],
        }),
    )

    suit_form_tabs = (
        ('general', 'Principale'),
        ('schedule', 'Programmazione')
    )

    def get_sites(self, obj):
        return ', '.join([str(s) for s in obj.sites.all()])
    get_sites.short_description = 'siti'

    def get_status(self, obj):
        schedules = obj.schedules.filter(
            from_date__lte=timezone.now(),
            to_date__gte=timezone.now()
        )
        if schedules.count():
            end = schedules.first().to_date
            delta = end - timezone.now().date()
            difference = str(delta).split(' ')[0]
            return mark_safe('''
                             <span style="background: #00cc00; display: inline-block;padding: 10px;border-radius: 50%; width: 18px;height:18px;text-align:center;">''' + # noqa
                             '''<i class="icon icon-ok"></i>
                             </span><br /><div>scade tra ''' + difference + ' giorni</div>') # noqa
        return mark_safe('''
                            <span style="background: #ff4444; display: inline-block;padding: 10px;border-radius: 50%; width: 18px;height:18px;text-align:center;">''' + # noqa
                            '''<i class="icon icon-off"></i>
                            </span>''') # noqa
    get_status.short_description = 'stato'

admin.site.register(Banner, BannerAdmin)


class ClickAdmin(admin.ModelAdmin):
    list_display = ('banner', 'site', 'datetime', 'ip', 'location', )
    list_display_links = None
    list_filter = ('banner', )

    def has_add_permission(self, request):
        return False

    def location(self, obj):
        gi = pygeoip.GeoIP('GeoLiteCity.dat')
        res = gi.record_by_addr(obj.ip)
        print res
        if res and res['city']:
            city = res['city']
        else:
            city = ''

        return city


admin.site.register(Click, ClickAdmin)
