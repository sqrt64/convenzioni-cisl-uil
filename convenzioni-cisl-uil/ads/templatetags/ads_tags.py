from django import template
from django.utils import timezone
from django.contrib.sites.shortcuts import get_current_site

from ..models import Banner

register = template.Library()


@register.inclusion_tag('ads/high_relevance.html', takes_context=True)
def ads_high_relevance(context):
    current_site = get_current_site(context['request'])
    try:
        main = Banner.objects.filter(relevance=Banner.MAIN_SPONSOR).filter(
            schedules__from_date__lte=timezone.now,
            schedules__to_date__gte=timezone.now,
            show_in_site=True,
            sites__in=[current_site]
        ).distinct()[0]
    except:
        main = None
    banners = Banner.objects.filter(relevance=Banner.HIGH_RELEVANCE).filter(
        schedules__from_date__lte=timezone.now,
        schedules__to_date__gte=timezone.now,
        show_in_site=True,
        sites__in=[current_site]
    ).order_by('position').distinct()

    return {'main': main, 'banners': banners}


@register.inclusion_tag('ads/low_relevance.html', takes_context=True)
def ads_low_relevance(context):
    current_site = get_current_site(context['request'])
    banners = Banner.objects.filter(relevance=Banner.LOW_RELEVANCE).filter(
        schedules__from_date__lte=timezone.now,
        schedules__to_date__gte=timezone.now,
        show_in_site=True,
        sites__in=[current_site]
    ).distinct()

    return {'banners': banners}
