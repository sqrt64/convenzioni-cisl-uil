from rest_framework import serializers

from .models import Banner


class BannerSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = Banner
        fields = ('id', 'client', 'url', 'image', )

    def get_image(self, banner):
        request = self.context.get('request')
        if banner.app_image:
            return request.build_absolute_uri(banner.app_image.url)
        return request.build_absolute_uri(banner.image.url)
