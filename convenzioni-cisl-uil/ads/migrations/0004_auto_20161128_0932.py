# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0003_auto_20161122_1616'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='relevance',
            field=models.IntegerField(verbose_name=b'rilevanza', choices=[(1, b'sponsor principale'), (2, b'alta'), (3, b'bassa')]),
        ),
    ]
