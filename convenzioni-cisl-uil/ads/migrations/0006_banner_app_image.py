# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0005_banner_position'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='app_image',
            field=models.ImageField(upload_to=b'ads/banner/app-img', null=True, verbose_name=b'immagine app', blank=True),
        ),
    ]
