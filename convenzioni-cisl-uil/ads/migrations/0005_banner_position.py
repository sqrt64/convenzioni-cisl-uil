# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0004_auto_20161128_0932'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='position',
            field=models.IntegerField(default=1, verbose_name=b'posizione'),
        ),
    ]
