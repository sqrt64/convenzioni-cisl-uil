# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0007_banner_show_in_app'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='show_in_site',
            field=models.BooleanField(default=True, verbose_name=b'mostra nel sito'),
        ),
    ]
