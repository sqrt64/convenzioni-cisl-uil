# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('ads', '0002_auto_20161122_1303'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='sites',
            field=models.ManyToManyField(to='sites.Site', verbose_name=b'siti'),
        ),
        migrations.AddField(
            model_name='click',
            name='site',
            field=models.ForeignKey(default=1, verbose_name=b'sito', to='sites.Site'),
            preserve_default=False,
        ),
    ]
