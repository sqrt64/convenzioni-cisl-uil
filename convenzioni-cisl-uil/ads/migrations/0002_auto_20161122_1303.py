# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='banner',
            field=models.ForeignKey(related_name='schedules', verbose_name=b'banner', to='ads.Banner'),
        ),
    ]
