# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0006_banner_app_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='show_in_app',
            field=models.BooleanField(default=False, verbose_name=b"mostra nell'app"),
        ),
    ]
