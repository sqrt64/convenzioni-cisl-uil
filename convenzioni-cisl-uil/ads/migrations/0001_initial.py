# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('client', models.CharField(max_length=255, verbose_name=b'cliente')),
                ('url', models.URLField(verbose_name=b'url')),
                ('image', models.ImageField(upload_to=b'ads/banner/img', verbose_name=b'immagine')),
                ('relevance', models.IntegerField(verbose_name=b'rilevanza', choices=[(1, b'alta'), (2, b'bassa')])),
            ],
            options={
                'verbose_name': 'Banner',
                'verbose_name_plural': 'Banner',
            },
        ),
        migrations.CreateModel(
            name='Click',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True, verbose_name=b'data e ora')),
                ('ip', models.CharField(max_length=15, null=True, verbose_name=b'ip', blank=True)),
                ('banner', models.ForeignKey(verbose_name=b'banner', to='ads.Banner')),
            ],
            options={
                'verbose_name': 'Click',
                'verbose_name_plural': 'Click',
            },
        ),
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('from_date', models.DateField(verbose_name=b'da')),
                ('to_date', models.DateField(verbose_name=b'a')),
                ('banner', models.ForeignKey(verbose_name=b'banner', to='ads.Banner')),
            ],
            options={
                'verbose_name': 'Programmazione',
                'verbose_name_plural': 'Programmazioni',
            },
        ),
    ]
