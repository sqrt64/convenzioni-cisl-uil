from django.db import models
from django.contrib.sites.models import Site


class Banner(models.Model):

    MAIN_SPONSOR = 1
    HIGH_RELEVANCE = 2
    LOW_RELEVANCE = 3

    RELEVANCE_CHOICES = (
        (MAIN_SPONSOR, 'sponsor principale'),
        (HIGH_RELEVANCE, 'alta'),
        (LOW_RELEVANCE, 'bassa'),
    )

    client = models.CharField('cliente', max_length=255)
    url = models.URLField('url')
    image = models.ImageField('immagine', upload_to='ads/banner/img')
    app_image = models.ImageField('immagine app',
                                  blank=True, null=True,
                                  upload_to='ads/banner/app-img')
    relevance = models.IntegerField('rilevanza', choices=RELEVANCE_CHOICES)
    position = models.IntegerField('posizione', default=1)
    sites = models.ManyToManyField(Site, verbose_name='siti')
    show_in_app = models.BooleanField('mostra nell\'app', default=False)
    show_in_site = models.BooleanField('mostra nel sito', default=True)

    class Meta:
        verbose_name = "Banner"
        verbose_name_plural = "Banner"

    def __unicode__(self):
        return self.client


class Schedule(models.Model):
    banner = models.ForeignKey(Banner, verbose_name='banner',
                               related_name='schedules')
    from_date = models.DateField('da')
    to_date = models.DateField('a')

    class Meta:
        verbose_name = "Programmazione"
        verbose_name_plural = "Programmazioni"

    def __unicode__(self):
        return '%s - (%s, %s)' % (
            self.banner.client,
            str(self.from_date),
            str(self.to_date)
        )


class Click(models.Model):
    banner = models.ForeignKey(Banner, verbose_name='banner')
    datetime = models.DateTimeField('data e ora', auto_now_add=True)
    site = models.ForeignKey(Site, verbose_name='sito')
    ip = models.CharField('ip', max_length=15, blank=True, null=True)

    class Meta:
        verbose_name = "Click"
        verbose_name_plural = "Click"

    def __unicode__(self):
        return '%s - click %s' % (
            self.banner.client,
            str(self.datetime)
        )
