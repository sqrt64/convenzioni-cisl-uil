from django.conf.urls import url
from .views import TraceUrl

urlpatterns = [
    url(r'^apri/(?P<pk>\d+)$', TraceUrl.as_view(), name='ads-trace-url'),
]
