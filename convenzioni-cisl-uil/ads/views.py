from django.shortcuts import redirect, get_object_or_404
from django.views.generic import View
from django.utils import timezone

from rest_framework import viewsets

from .models import Banner, Click
from .serializers import BannerSerializer


class TraceUrl(View):
    def get(self, request, pk):
        banner = get_object_or_404(Banner, id=pk)
        click = Click(banner=banner, ip=self.get_client_ip(request),
                      site=request.site)
        click.save()
        return redirect(banner.url)

    def get_client_ip(self, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip


# API
class BannerViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to fetch banners
    """
    queryset = Banner.objects.all()
    serializer_class = BannerSerializer

    def get_queryset(self):
        return Banner.objects.filter(
            schedules__from_date__lte=timezone.now,
            schedules__to_date__gte=timezone.now,
            show_in_app=True,
            sites__in=[self.request.site]
        ).order_by('position').distinct()
