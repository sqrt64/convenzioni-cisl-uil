# -*- coding: utf-8 -*-
import operator
from collections import OrderedDict

from django.db import models
from django.contrib.sites.models import Site
from django.db.models import Q
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site

from ckeditor_uploader.fields import RichTextUploadingField
from taggit.managers import TaggableManager

from .managers import EntryManager


class Entry(models.Model):
    """ Blog Entry
    """
    STATUS_DRAFT = 0
    STATUS_PUBLISHED = 1

    STATUS_CHOICES = (
        (STATUS_DRAFT, 'bozza'),
        (STATUS_PUBLISHED, 'pubblicato'),
    )

    insertion_date = models.DateTimeField('inserimento', auto_now_add=True)
    last_edit_date = models.DateTimeField('ultima modifica', auto_now=True)
    author = models.CharField('autore', max_length=50, default='Elio Dogliotti')
    title = models.CharField('titolo', max_length=128)
    slug = models.SlugField('slug', max_length=128)
    text = RichTextUploadingField('testo')
    image = models.ImageField('immagine', upload_to='blog/img/')
    tags = TaggableManager('tag', help_text='valori separati da virgole', blank=True) # noqa
    status = models.IntegerField(verbose_name='status', choices=STATUS_CHOICES)
    enable_comments = models.BooleanField('abilita commenti', default=True)
    featured = models.BooleanField('featured', default=False)
    visits = models.IntegerField('visite', blank=True, default=0)
    related = models.ManyToManyField("self", verbose_name='articoli correlati', blank=True) # noqa
    sites = models.ManyToManyField(Site, verbose_name='siti')
    # meta
    meta_title = models.CharField('meta titolo', max_length=128, blank=True, null=True, help_text='default: titolo') # noqa
    meta_description = models.TextField('meta descrizione', blank=True, null=True, help_text='default: porzione di testo') # noqa
    meta_keywords = models.CharField('meta parole chiavi', max_length=128, blank=True, null=True, help_text='default: tag') # noqa
    meta_image = models.ImageField('meta immagine', upload_to='blog/img/', blank=True, null=True, help_text='default: imamgine') # noqa

    objects = EntryManager()

    class Meta:
        verbose_name = 'articolo'
        verbose_name_plural = 'articoli'
        ordering = ('-insertion_date',)

    def __unicode__(self):
        return '%s' % self.title

    def get_time_attribute(self):
        return self.insertion_date.strftime("%Y-%m-%dT%H:%M:%S%Z+01:00")

    @models.permalink
    def get_absolute_url(self):
        return ('blog-detail', None, {
            'slug': self.slug,
            'year': self.insertion_date.year,
            'month': self.insertion_date.strftime("%m"),
            'day': self.insertion_date.strftime("%d")
        })

    def get_full_absolute_url(self):
        current_site = Site.objects.get_current()
        return ''.join(['http://',
                        current_site.domain,
                        self.get_absolute_url()])

    def image_abs(self):
        current_site = get_current_site(None)
        return ''.join(
            ['http://', current_site.domain,
             self.image.url])

    def get_related(self):
        # first things first: marked as related
        related = [e for e in self.related.all()]
        # search for tag in titles
        tags = [t.name for t in self.tags.all()]
        if len(tags) == 0:
            return []
        query = reduce(operator.or_, (Q(title__icontains=item) for item in tags)) # noqa
        in_title = Entry.objects.published().filter(query).exclude(id=self.id)
        # and in tags
        query = reduce(operator.or_, (Q(tags__name__in=[item]) for item in tags)) # noqa
        in_tags = Entry.objects.published().filter(query).exclude(id=self.id)

        res = list(OrderedDict.fromkeys(related + [e for e in in_title] + [e for e in in_tags])) # noqa
        return res[:3]
