from django.db import models


class EntryManager(models.Manager):

    def published(self, **kwargs):
        from blog.models import Entry
        return self.filter(status=Entry.STATUS_PUBLISHED, **kwargs)

    def featured(self, **kwargs):
        return self.published(featured=True, **kwargs)
