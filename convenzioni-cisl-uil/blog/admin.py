# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Entry


class EntryAdmin(admin.ModelAdmin):
    list_display = [
        'insertion_date',
        'title',
        'featured',
        'status',
        'get_sites',
    ]
    list_filter = (
        'insertion_date',
        'featured',
        'status',
        'sites',
    )
    list_editable = (
        'featured',
        'status'
    )
    search_fields = (
        'title',
        'text',
    )
    prepopulated_fields = {
        'slug': ('title', ),
    }
    raw_id_fields = ('related', )
    related_lookup_fields = {
        'm2m': ['related'],
    }

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-main',),
            'fields': [
                'title',
                'slug',
                'text',
                'image',
                'tags',
                'related',
                'status',
                'sites',
            ],
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-flags',),
            'fields': [
                'enable_comments',
                'featured',
            ],
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-meta',),
            'fields': [
                'meta_title',
                'meta_description',
                'meta_keywords',
                'meta_image',
            ],
        }),
    )

    suit_form_tabs = (
        ('main', 'Principale'),
        ('flags', 'Flags'),
        ('meta', 'Meta & Social')
    )

    def get_sites(self, obj):
        return ', '.join([str(s) for s in obj.sites.all()])

    get_sites.short_description = 'siti'


admin.site.register(Entry, EntryAdmin)
