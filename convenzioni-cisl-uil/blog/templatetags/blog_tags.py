from django import template
from django.contrib.sites.shortcuts import get_current_site

from ..models import Entry

register = template.Library()


@register.inclusion_tag('blog/featured.html', takes_context=True)
def blog_featured(context):
    current_site = get_current_site(context['request'])
    entries = Entry.objects.featured(sites__in=[current_site])
    return {'entry': entries.first()}


@register.inclusion_tag('blog/blog_archive.html', takes_context=True)
def blog_archive(context):
    current_site = get_current_site(context['request'])
    entries = Entry.objects.published(
        sites__in=[current_site]).order_by('-insertion_date')
    return {'entries': entries}
