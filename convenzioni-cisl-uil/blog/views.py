from django.views.generic import DetailView, ListView, View
from django.shortcuts import render, redirect
from django.db.models import Q

from blog.models import Entry


class EntryDetailView(DetailView):
    model = Entry

    def get_queryset(self):
        return Entry.objects.published(sites__in=[self.request.site])

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(EntryDetailView, self).get_context_data(**kwargs)
        context['base_url'] = self.request.build_absolute_uri("/").rstrip("/")

        return context


class CategoryListView(ListView):
    model = Entry
    template_name = 'blog/category_list.html'
    paginate_by = 5

    def get_queryset(self):
        tag = self.kwargs['tag']
        return Entry.objects.published(sites__in=[self.request.site]).filter(tags__name__in=[tag]).order_by('-insertion_date') # noqa

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(CategoryListView, self).get_context_data(**kwargs)
        context['tag'] = self.kwargs['tag']

        return context


class ArchiveView(ListView):
    model = Entry
    template_name = 'blog/archive.html'
    paginate_by = 10

    def get_queryset(self):
        # return Entry.objects.published(relevance=Entry.LOW_RELEVANCE).order_by('-featured', '-insertion_date') # noqa
        return Entry.objects.published(sites__in=[self.request.site]).order_by('-featured', '-insertion_date') # noqa


class SearchView(View):
    def get(self, request):
        return render(
            request,
            'blog/search.html',
            {
                'text': request.session.get('mvs-text', ''),
            }
        )

    def post(self, request):
        text = request.POST.get('text')
        request.session['mvs-text'] = text

        return redirect('/blog/ricerca/risultati/')


class SearchResultView(ListView):
    model = Entry
    template_name = 'blog/search_result.html'
    paginate_by = 16

    def get_context_data(self, **kwargs):
        context = super(SearchResultView, self).get_context_data(**kwargs)
        context['text'] = self.request.session.get('mvs-text', '')

        return context

    def get_queryset(self):
        request = self.request
        entries = Entry.objects.published(sites__in=[self.request.site])
        if request.session.get('mvs-text', ''):
            t = request.session['mvs-text']
            entries = entries.filter(
                Q(title__icontains=t) | Q(text__icontains=t) | Q(tags__name__in=[t]) # noqa
            )

        return entries.distinct().order_by('relevance')
