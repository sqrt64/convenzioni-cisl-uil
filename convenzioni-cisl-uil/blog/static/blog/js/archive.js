window.BlogArchive = function (options) {

    this.init = function (options) {
        this.itemsForPage = options.itemsForPage;
        this.entries = options.entries;
        this.steps = options.steps;
        this.zones = options.zones;

        this.prepareItems();
        // pagination
        this.start = 0;
        this.numPages = Math.ceil(this.items.length / this.itemsForPage);

        var page = /p[0-9]+/.test(location.hash) ? location.hash.substr(2) : 1;
        this.gotoPage(page, true);
    }

    this.prepareItems = function () {
        this.items = [];
        var currentStep = 0;
        var previousIndex = 0;
        var steps = [2, 2, 2, 5];
        var currentZone = 0;
        var self = this;
        jQuery.each(this.entries, function (index, e) {
            self.items.push({ type: 'entry', value: e });
            if (index + 1 - previousIndex === self.steps[currentStep]) {
                previousIndex = index + 1;
                currentStep = (currentStep + 1) >= steps.length ? 0 : currentStep + 1
                self.items.push({ type: 'ads', value: self.zones[currentZone] });
                currentZone = currentZone === self.zones.length - 1 ? 0 : currentZone + 1;
            }
        });
    }

    this.renderPage = function () {
        var els = [];
        var counter = 0;
        for (var i = this.start, l = this.start + this.itemsForPage; i < l; i++) {
            counter++;
            if (this.items[i]) {
                var item = this.items[i];
                var content;
                if (item.type === 'entry') {
                    var e = item.value;
                    content = '<div class="col-md-6 col-lg-3 low-relevance-entry">';
                    content += '<div class="card" onclick="location.href=\'' + e.url + '\'">';
                    content += '<img class="img-responsive" src="' + e.imageUrl + '">';
                    content += '<div class="card-block">';
                    content += '<h4 class="card-title text-center">' + e.title + '</h4>';
                    if (e.author) {
                        content += '<h6 class="text-center">di ' + e.author + '</h6>';
                    }
                    // content += '<div class="text-center card-body">' + e.text + '</div>';
                    content += '</div>';
                    content += '</div>';
                    content += '</div>';
                } else {
                    var zone = item.value;
                    content = '<div class="col-md-6 col-lg-3 low-relevance-entry">';
                    content += '<div class="ads-zone ads-zone-' + zone + '"><div id="ads' + zone + '"></div></div>';
                    content += '<script>jQuery.ajax({url: "/ads/zone/' + zone + '"}).done(function(res) { $(\'#ads' + zone + '\').replaceWith(res) })<\/script>';
                    content += '</div>';
                }

                if (counter%2 === 0) {
                    content += '<div class="clearfix hidden-lg-up"></div>';
                }
                if (counter%4 === 0) {
                    content += '<div class="clearfix hidden-md-down"></div>';
                }
                els.push(content);
            }
        }
        $('#low-relevance-entries').html(els.join(''))
    }

    this.pagination = function () {
        if (this.numPages === 1) return;

        var currentPage = Math.ceil(this.start / this.itemsForPage) + 1;
        var prevDisabled = currentPage === 1;
        var nextDisabled = currentPage === this.numPages;

        $('#archive-anchor').attr('name', 'p' + currentPage);

        var pag = '<ul class="pagination">';
        if (prevDisabled) {
            pag += '<li class="disabled"><span title="Previous Page">←</span></li>';
        } else {
            pag += '<li class=""><span onclick="window.archiveInstance.prev()" title="Previous Page">←</span></li>';
        }
        for (var i = 0; i < this.numPages; i++) {
            if ( currentPage === i + 1 ) {
                pag += '<li class="active"><span title="Current Page"> ' + (i + 1) + ' </span></li>';
            } else {
                pag += '<li class=""><span onclick="window.archiveInstance.gotoPage(' + (i + 1) + ')" title="Page ' + (i + 1) + '"> ' + (i + 1) + ' </span></li>';
            }
        }
        if (nextDisabled) {
            pag += '<li class="disabled"><span title="Next Page">→</span></li>';
        } else {
            pag += '<li class=""><span onclick="window.archiveInstance.next()" title="Next Page">→</span></li>';
        }
        pag += '</ul>'
        $('#pagination').html(pag)
    }

    this.gotoPage = function (page, skipSetHash) {
        this.start = page * this.itemsForPage - this.itemsForPage;
        this.renderPage();
        this.pagination();
        if (!skipSetHash && page !== 1) {
            location.hash = 'p' + page;
        } else {
            location.hash = '';
        }
    }

    this.prev = function () {
        this.start = this.start - this.itemsForPage;
        this.renderPage();
        this.pagination();
        var page = Math.ceil(this.start / this.itemsForPage) + 1;
        if (page !== 1) {
            location.hash = 'p' + page
        } else {
            location.hash = '';
        }
    }

    this.next = function () {
        this.start = this.start + this.itemsForPage;
        this.renderPage();
        this.pagination();
        var page = Math.ceil(this.start / this.itemsForPage) + 1;
        if (page !== 1) {
            location.hash = 'p' + page;
        } else {
            location.hash = '';
        }
    }

    this.init(options)
}
