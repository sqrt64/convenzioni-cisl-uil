from django.conf.urls import url
from blog.views import EntryDetailView, CategoryListView, ArchiveView, SearchView, SearchResultView # noqa

urlpatterns = [
    url(r'^$', ArchiveView.as_view(), name='blog-archive'),
    url(r'^ricerca/$', SearchView.as_view(), name='blog-search'),
    url(r'^ricerca/risultati/$', SearchResultView.as_view(), name='blog-search-result'),
    url(r'^categoria/(?P<tag>[-\w .]+)/$', CategoryListView.as_view(), name='blog-category-list'),
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$', EntryDetailView.as_view(), name='blog-detail'),
]
