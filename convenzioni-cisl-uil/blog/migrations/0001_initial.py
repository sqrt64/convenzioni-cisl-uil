# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import taggit.managers
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('insertion_date', models.DateTimeField(auto_now_add=True, verbose_name=b'inserimento')),
                ('last_edit_date', models.DateTimeField(auto_now=True, verbose_name=b'ultima modifica')),
                ('title', models.CharField(max_length=128, verbose_name=b'titolo')),
                ('slug', models.SlugField(max_length=128, verbose_name=b'slug')),
                ('text', ckeditor_uploader.fields.RichTextUploadingField(verbose_name=b'testo')),
                ('image', models.ImageField(upload_to=b'blog/img/', verbose_name=b'immagine')),
                ('status', models.IntegerField(verbose_name=b'status', choices=[(0, b'bozza'), (1, b'pubblicato')])),
                ('enable_comments', models.BooleanField(default=True, verbose_name=b'abilita commenti')),
                ('featured', models.BooleanField(default=False, verbose_name=b'featured')),
                ('visits', models.IntegerField(default=0, verbose_name=b'visite', blank=True)),
                ('meta_title', models.CharField(help_text=b'default: titolo', max_length=128, null=True, verbose_name=b'meta titolo', blank=True)),
                ('meta_description', models.TextField(help_text=b'default: porzione di testo', null=True, verbose_name=b'meta descrizione', blank=True)),
                ('meta_keywords', models.CharField(help_text=b'default: tag', max_length=128, null=True, verbose_name=b'meta parole chiavi', blank=True)),
                ('meta_image', models.ImageField(help_text=b'default: imamgine', upload_to=b'blog/img/', null=True, verbose_name=b'meta immagine', blank=True)),
                ('related', models.ManyToManyField(related_name='_related_+', verbose_name=b'articoli correlati', to='blog.Entry', blank=True)),
                ('sites', models.ManyToManyField(to='sites.Site', verbose_name=b'siti')),
                ('tags', taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text=b'valori separati da virgole', verbose_name=b'tag')),
            ],
            options={
                'ordering': ('-insertion_date',),
                'verbose_name': 'articolo',
                'verbose_name_plural': 'articoli',
            },
        ),
    ]
