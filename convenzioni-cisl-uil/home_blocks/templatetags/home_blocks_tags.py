from django import template
from django.utils import timezone
from django.contrib.sites.shortcuts import get_current_site
from django.db.models import Q

from ..models import Block

register = template.Library()


@register.inclusion_tag('home_blocks/block.html', takes_context=True)
def home_block(context):
    current_site = get_current_site(context['request'])
    try:
        block = Block.objects.filter(
            Q(date_to__isnull=True) | Q(date_to__gte=timezone.now()),
            date_from__lte=timezone.now(),
            published=True,
            sites__in=[current_site]
        )[0]
    except:  # noqa
        block = None

    return {'block': block}
