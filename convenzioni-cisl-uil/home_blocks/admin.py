from django.contrib import admin

from .models import Block


class BlockAdmin(admin.ModelAdmin):
    list_display = ('title', 'date_from', 'date_to', 'published', )


admin.site.register(Block, BlockAdmin)
