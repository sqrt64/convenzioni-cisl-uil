# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Block',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=128, verbose_name=b'titolo')),
                ('text', ckeditor_uploader.fields.RichTextUploadingField(verbose_name=b'testo')),
                ('date_from', models.DateField(default=django.utils.timezone.now, verbose_name=b'da')),
                ('date_to', models.DateField(null=True, verbose_name=b'a', blank=True)),
                ('published', models.BooleanField(verbose_name=b'pubblicata')),
                ('sites', models.ManyToManyField(to='sites.Site', verbose_name=b'siti')),
            ],
            options={
                'verbose_name': 'Blocco',
                'verbose_name_plural': 'Blocchi',
            },
        ),
    ]
