from django.db import models
from django.utils import timezone
from django.contrib.sites.models import Site

from ckeditor_uploader.fields import RichTextUploadingField


class Block(models.Model):
    title = models.CharField('titolo', max_length=128)
    text = RichTextUploadingField(verbose_name='testo')
    date_from = models.DateField('da', default=timezone.now)
    date_to = models.DateField('a', blank=True, null=True)
    published = models.BooleanField('pubblicata')
    sites = models.ManyToManyField(Site, verbose_name='siti')

    class Meta:
        verbose_name = "Blocco"
        verbose_name_plural = "Blocchi"

    def __str__(self):
        return '%s' % self.title
