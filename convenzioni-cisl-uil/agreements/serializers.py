from rest_framework import serializers

from .models import Category, Company, Point


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'slug',
        )


class CompanySerializer(serializers.ModelSerializer):
    tags = serializers.SerializerMethodField()
    images = serializers.SerializerMethodField()
    videos = serializers.SerializerMethodField()
    attachments = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = (
            'id',
            'name',
            'categories',
            'slug',
            'summary',
            'text',
            'phone',
            'phone2',
            'email',
            'web',
            'fb',
            'relevance',
            'tags',
            'images',
            'videos',
            'attachments',
        )

    def get_tags(self, company):
        return [t.name for t in company.tags.all()]

    def get_images(self, company):
        request = self.context.get('request')
        images = [
            request.build_absolute_uri(image.file.url)
            for image in company.images.all()
        ]  # noqa
        return images

    def get_videos(self, company):
        request = self.context.get('request')
        videos = [
            {
                'title': video.title,
                'type': video.get_type_display(),
                'code': video.code,
                'width': video.width,
                'height': video.height,
                'thumb': request.build_absolute_uri(video.thumb.url),
                'watchUrl': video.watch_url
            }
            for video in company.videos.filter(sites__in=[request.site])
        ]  # noqa
        return videos

    def get_attachments(self, company):
        request = self.context.get('request')
        attachments = [
            {
                'name': attachment.name,
                'url': request.build_absolute_uri(attachment.file.url),
                'extension': attachment.extension,
                'size': attachment.size,
            }
            for attachment in company.attachments.all()
        ]  # noqa
        return attachments


class PointSerializer(serializers.ModelSerializer):
    company = CompanySerializer()
    region = serializers.SerializerMethodField()
    province = serializers.SerializerMethodField()
    city = serializers.SerializerMethodField()

    class Meta:
        model = Point
        fields = (
            'id',
            'company',
            'address',
            'cap',
            'region',
            'province',
            'city',
            'lat',
            'lng',
            'phone',
            'email',
            'web',
            'text'
        )

    def get_region(self, point):
        return point.region.name

    def get_province(self, point):
        return point.province.name

    def get_city(self, point):
        return point.city.name
