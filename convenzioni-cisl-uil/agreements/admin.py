# -*- coding: utf-8 -*-
import datetime

from django.contrib import admin
from django.db.models import F
from django.utils.html import mark_safe

from .models import (Attachment, Category, City, Company, Image, Point,  # noqa
                     Proposal, Province, Region, Video, CouponRegistration, SpamIndicator)


class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'get_sites',
    )
    prepopulated_fields = {
        'slug': ('name', ),
    }

    def get_sites(self, obj):
        return ', '.join([str(s) for s in obj.sites.all()])

    get_sites.short_description = 'siti'


admin.site.register(Category, CategoryAdmin)


class RegionAdmin(admin.ModelAdmin):
    list_display = ('name', )


admin.site.register(Region, RegionAdmin)


class ProvinceAdmin(admin.ModelAdmin):
    list_display = (
        'abbreviation',
        'region',
    )
    list_filter = ('region', )


admin.site.register(Province, ProvinceAdmin)


class CityAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'province',
    )


admin.site.register(City, CityAdmin)


class ImageInline(admin.StackedInline):
    model = Image
    extra = 1
    suit_classes = 'suit-tab suit-tab-images'


class VideoInline(admin.StackedInline):
    model = Video
    extra = 1
    suit_classes = 'suit-tab suit-tab-videos'


class AttachmentInline(admin.StackedInline):
    model = Attachment
    extra = 1
    suit_classes = 'suit-tab suit-tab-attachments'


def add_one_year_validity(modeladmin, request, queryset):
    queryset.update(date_to=F('date_to') + datetime.timedelta(days=365))


add_one_year_validity.short_description = "Prolunga validità di un anno"


def hide_contact_form(modeladmin, request, queryset):
    queryset.update(contact_form=False)


hide_contact_form.short_description = "Nascondi form di contatto"


def show_contact_form(modeladmin, request, queryset):
    queryset.update(contact_form=True)


show_contact_form.short_description = "Mostra form di contatto"


class CompanyAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'relevance',
        'get_categories',
        'date_from',
        'date_to',
        'visibility_italy',
        'contact_form',
        'coupon_form',
        'show_in_app',
        'get_sites',
    )  # noqa
    list_filter = (
        'categories',
        'date_from',
        'date_to',
    )  # noqa
    search_fields = ('name', )
    list_editable = ('relevance', 'show_in_app', )
    prepopulated_fields = {
        'slug': ('name', ),
    }
    inlines = (
        ImageInline,
        VideoInline,
        AttachmentInline,
    )
    actions = [add_one_year_validity, show_contact_form, hide_contact_form]

    fieldsets = (
        (None, {
            'classes': (
                'suit-tab',
                'suit-tab-general',
            ),
            'fields': [
                'categories',
                'name',
                'slug',
                'relevance',
                'tags',
                'summary',
                'text',
                'textual_address',
                'phone',
                'mobile',
                'whatsapp',
                'email',
                'web',
                'fb',
                'contact_form',
                'coupon_form',
                'coupon_form_text',
                'coupon_btn_text',
                'slideshow',
                'visibility_italy',
                'show_in_app',
                'sites',
                'proposal',
            ],
        }),
        (None, {
            'classes': (
                'suit-tab',
                'suit-tab-validity',
            ),
            'fields': [
                'date_from',
                'date_to',
            ],
        }),
    )

    suit_form_tabs = (
        ('general', 'Principale'),
        ('validity', 'Validità'),
        ('images', 'Immagini'),
        ('videos', 'Video'),
        ('attachments', 'Allegati'),
    )

    def get_sites(self, obj):
        return ', '.join([str(s) for s in obj.sites.all()])

    get_sites.short_description = 'siti'

    def get_categories(self, obj):
        return ', '.join([str(s) for s in obj.categories.all()])

    get_categories.short_description = 'categorie'


admin.site.register(Company, CompanyAdmin)


class PointAdmin(admin.ModelAdmin):
    list_display = (
        'company',
        'address',
        'city',
        'province',
        'region',
        'get_visibility',
    )  # noqa
    list_filter = (
        'company',
        'region',
        'province',
    )  # noqa
    filter_horizontal = ('visibility_provinces', )
    fieldsets = (
        (None, {
            'classes': (
                'suit-tab',
                'suit-tab-general',
            ),
            'fields': [
                'company',
                'phone',
                'mobile',
                'whatsapp',
                'email',
                'web',
                'text',
                'visibility_provinces',
            ],
        }),
        (None, {
            'classes': (
                'suit-tab',
                'suit-tab-location',
            ),
            'fields': [
                'address',
                'cap',
                'region',
                'province',
                'city',
                'lat',
                'lng',
            ],
        }),
    )

    suit_form_tabs = (
        ('general', 'Principale'),
        ('location', 'Localizzazione'),
    )

    def get_visibility(self, obj):
        if obj.company.visibility_italy:
            return 'tutta Italia'
        res = []
        vis_prov = [x.name for x in obj.visibility_provinces.all()]
        res += vis_prov
        res += [obj.province.name]
        return ', '.join(res)

    get_visibility.short_description = 'visibilità'


admin.site.register(Point, PointAdmin)


class ProposalAdmin(admin.ModelAdmin):
    list_display = (
        'date',
        'site',
        'firstname',
        'lastname',
        'company_name',
        'company_region',
        'getactions',
    )  # noqa
    list_filter = ('date', )

    def getactions(self, obj):
        has_company = False
        try:
            has_company = True if obj.company.pk else False
        except:
            pass
        if not has_company:
            return mark_safe(
                '<a href="/admin/agreements/company/add/?proposal=%s&name=%s&categories=%s&phone=%s&email=%s&web=%s&text=%s" class="btn btn-primary">crea azienda</a>'
                % (  # noqa
                    obj.pk,
                    obj.company_name,
                    obj.company_category.id if obj.company_category else '',
                    obj.company_phone,
                    obj.company_email,
                    obj.company_web,
                    obj.company_description,
                ))
        else:
            return mark_safe(
                '<a href="/admin/agreements/point/add/?company=%s&address=%s&region=%s&province=%s&city=%s" class="btn btn-primary">crea punto convenzionato</a>'
                % (  # noqa
                    obj.company.id,
                    obj.company_address,
                    obj.company_region.id if obj.company_region else '',
                    obj.company_province.id if obj.company_province else '',
                    obj.company_city.id if obj.company_city else '',
                ))

    getactions.short_description = 'Azioni'


admin.site.register(Proposal, ProposalAdmin)


class CouponRegistrationAdmin(admin.ModelAdmin):
    list_display = ('firstname', 'lastname', 'email', 'cap', 'company', 'gdpr_dt', 'site', )
    list_filter = ('site', )
    search_fields = ('lastname', 'email', )


admin.site.register(CouponRegistration, CouponRegistrationAdmin)


@admin.register(SpamIndicator)
class SpamIndicatorAdmin(admin.ModelAdmin):
    list_display = ('name', 'pattern', 'ignore_case', 'dot_matches_newline', 'active', )
    list_editable = ('active', )
