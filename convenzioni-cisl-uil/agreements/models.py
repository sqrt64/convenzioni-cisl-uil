# -*- coding: utf-8 -*-
import json
import os
import urllib

from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.core.files import File
from django.db import models
from django.utils import timezone
from smart_selects.db_fields import ChainedForeignKey
from taggit.managers import TaggableManager

from .managers import ImageManager, VideoManager


class Category(models.Model):
    name = models.CharField('nome', max_length=200)
    slug = models.SlugField('slug', unique=True)
    old_id = models.IntegerField('vecchio ID', blank=True, null=True)
    sites = models.ManyToManyField(Site, verbose_name='siti')

    class Meta:
        verbose_name = "Categoria"
        verbose_name_plural = "Categorie"
        ordering = ('name', )

    def __unicode__(self):
        return self.name


class Region(models.Model):
    name = models.CharField('nome', max_length=200)

    class Meta:
        verbose_name = "Regione"
        verbose_name_plural = "Regioni"
        ordering = ('name', )

    def __unicode__(self):
        return self.name


class Province(models.Model):
    region = models.ForeignKey(
        Region, verbose_name='regione', related_name='provinces')
    name = models.CharField('nome', max_length=200)
    abbreviation = models.CharField('sigla', max_length=2)

    class Meta:
        verbose_name = "Provincia"
        verbose_name_plural = "Province"
        ordering = ('name', )

    def __unicode__(self):
        return self.name


class City(models.Model):
    province = models.ForeignKey(
        Province, verbose_name='provincia', related_name='cities')
    name = models.CharField('nome', max_length=200)

    class Meta:
        verbose_name = "Comune"
        verbose_name_plural = "Comuni"
        ordering = ('name', )

    def __unicode__(self):
        return self.name


class Company(models.Model):
    proposal = models.OneToOneField(
        'Proposal',
        verbose_name='suggerimento',
        blank=True,
        null=True,
        related_name='company')  # noqa
    categories = models.ManyToManyField(
        Category, verbose_name='categorie', related_name='points')
    name = models.CharField('nome', max_length=200)
    slug = models.SlugField('slug')
    summary = RichTextUploadingField(verbose_name='descrizione breve')
    text = RichTextUploadingField(verbose_name='testo')
    textual_address = RichTextUploadingField(
        verbose_name='indirizzo testuale', blank=True, null=True)
    phone = models.CharField('telefono', max_length=50, blank=True, null=True)
    phone2 = models.CharField(
        'altro telefono', max_length=50, blank=True, null=True)
    mobile = models.CharField('cellulare', max_length=50, blank=True, null=True)
    whatsapp = models.CharField('numero whatsapp', max_length=50, blank=True, null=True)
    email = models.EmailField('email', blank=True, null=True)
    web = models.URLField('web', blank=True, null=True)
    fb = models.URLField('pagina facebook', blank=True, null=True)
    date_from = models.DateField('da', default=timezone.now)
    date_to = models.DateField('a', blank=True, null=True)
    tags = TaggableManager()
    relevance = models.IntegerField('priorità')
    visibility_italy = models.BooleanField(
        'visibile in tutta Italia', default=False)
    contact_form = models.BooleanField('form di contatto')
    coupon_form = models.BooleanField('form di richiesta codici sconto',
                                      default=False)
    coupon_form_text = models.TextField('testo mostrato prima del form', default='''Compila il modulo in ogni sua parte per richiedere il tuo codice sconto: te lo invieremo al più presto all'email da te indicata o tramite messaggio whatsapp. Alla stessa email invieremo, mensilmente, notizie su nuove convenzioni e promozioni speciali riservate agli iscritti.''', blank=True, null=True)
    coupon_btn_text = RichTextUploadingField(verbose_name='testo sopra bottone richiedi sconto', blank=True, null=True)
    slideshow = models.BooleanField(
        'attiva slideshow',
        help_text='dimensioni ottimali immagini: 650x325',
        default=False)
    show_in_app = models.BooleanField('mostra nell\'app', default=True)
    sites = models.ManyToManyField(Site, verbose_name='siti')

    class Meta:
        verbose_name = 'Azienda'
        verbose_name_plural = 'Aziende'
        ordering = ('name', )

    def __unicode__(self):
        return '%s' % (self.name)

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse(
            'agreements-detail', args=[
                self.slug,
                self.pk,
            ])

    def get_full_absolute_url(self):
        current_site = get_current_site(None)
        return ''.join(
            ['http://', current_site.domain,
             self.get_absolute_url()])

    def categories_txt(self):
        return ', '.join([x.name for x in self.categories.all()])

    def unique(self, list1):
        # insert the list to the set
        list_set = set(list1)
        # convert the set to the list
        unique_list = (list(list_set))
        return unique_list

    def cities_txt(self):
        return ', '.join(self.unique([str(x.city) for x in self.points.all()]))

    def image(self):
        if self.images.site().count():
            try:
                return self.images.site().filter(use_in_card=True).first().file
            except:
                return None
        return None

    def image_abs(self):
        if self.image() is None:
            return ''
        current_site = get_current_site(None)
        return ''.join(
            ['http://', current_site.domain,
             self.image().url])

    def has_expired(self):
        if timezone.now().date() < self.date_from:
            return True

        if self.date_to is not None and self.date_to < timezone.now().date():
            return True


class Point(models.Model):
    company = models.ForeignKey(
        Company, verbose_name='azienda', related_name='points')  # noqa
    address = models.CharField('indirizzo', max_length=180)
    cap = models.CharField('cap', max_length=5)
    region = models.ForeignKey(
        Region, verbose_name='regione', related_name='points')
    province = ChainedForeignKey(
        Province,
        chained_field="region",
        chained_model_field="region",
        show_all=False,
        auto_choose=True,
        sort=True,
        verbose_name='provincia',
    )
    city = ChainedForeignKey(
        City,
        chained_field="province",
        chained_model_field="province",
        show_all=False,
        auto_choose=True,
        sort=True,
        verbose_name='città',
    )
    lat = models.CharField('latitudine', max_length=50, blank=True, null=True)
    lng = models.CharField('longitudine', max_length=50, blank=True, null=True)
    phone = models.CharField('telefono', max_length=50, blank=True, null=True)
    mobile = models.CharField('cellulare', max_length=50, blank=True, null=True)
    whatsapp = models.CharField('numero whatsapp', max_length=50, blank=True, null=True)
    email = models.EmailField('email', blank=True, null=True)
    web = models.URLField('web', blank=True, null=True)
    text = RichTextUploadingField(verbose_name='testo', blank=True, null=True)
    visibility_provinces = models.ManyToManyField(
        Province,
        verbose_name=u'province di visibilità',
        related_name='visibility_points',
        blank=True,
        help_text=
        'se compilato sarà visibile nella sua provincia e quelle selezionate qui. ',  # noqa
    )

    class Meta:
        verbose_name = "Punto convenzionato"
        verbose_name_plural = "Punti convenzionati"

    def __unicode__(self):
        return '%s, %s' % (self.address, self.city.name)


class Image(models.Model):
    """ Image model
    """
    company = models.ForeignKey(
        Company, verbose_name='azienda', related_name='images')
    title = models.CharField('titolo', max_length=255)
    file = models.ImageField(upload_to='agreements/point/img/', max_length=100)
    date_creation = models.DateTimeField('inserimento', auto_now_add=True)
    last_edit = models.DateTimeField('ultima modifica', auto_now=True)
    sites = models.ManyToManyField(Site, verbose_name='siti')
    use_in_card = models.BooleanField('Usa nella scheda', default=False)

    objects = ImageManager()

    class Meta:
        verbose_name = "Immagine"
        verbose_name_plural = "Immagini"

    def __unicode__(self):
        return '%s' % self.title


class Video(models.Model):
    """ Video model
        These are video hosted on external platforms: youtube, vimeo
    """
    YOUTUBE_TYPE = 1
    VIMEO_TYPE = 2
    TYPE_CHOICES = (
        (YOUTUBE_TYPE, 'youtube'),
        (VIMEO_TYPE, 'vimeo'),
    )

    company = models.ForeignKey(
        Company, verbose_name='azienda', related_name='videos')
    title = models.CharField('titolo', max_length=255)
    type = models.IntegerField('piattaforma', choices=TYPE_CHOICES)
    code = models.CharField('codice', max_length=64)
    width = models.IntegerField('larghezza')
    height = models.IntegerField('altezza')
    thumb = models.ImageField(
        upload_to='agreements/point/video',
        max_length=100,
        blank=True,
        null=True,
        help_text=
        'se non inserita viene utilizzata una thumb fornita dalla piattaforma'
    )  # noqa
    date_creation = models.DateTimeField('inserimento', auto_now_add=True)
    last_edit = models.DateTimeField('ultima modifica', auto_now=True)
    sites = models.ManyToManyField(Site, verbose_name='siti')

    objects = VideoManager()

    class Meta:
        verbose_name = "Video"
        verbose_name_plural = "Video"

    def __unicode__(self):
        return '%s' % self.title

    def save(self):
        if not self.thumb:
            if self.type == self.YOUTUBE_TYPE:
                url = 'http://img.youtube.com/vi/%s/0.jpg' % self.code
            elif self.type == self.VIMEO_TYPE:
                response = urllib.urlopen(
                    'http://vimeo.com/api/v2/video/%s.json' %
                    self.code)  # noqa
                data = json.loads(response.read().decode('utf8'))
                url = data[0].get('thumbnail_large')
            result = urllib.urlretrieve(url)
            self.thumb.save(os.path.basename(url), File(open(result[0])))
        return super(Video, self).save()

    @property
    def watch_url(self):
        if self.type == self.YOUTUBE_TYPE:
            return 'https://www.youtube.com/watch?v=%s' % self.code
        elif self.type == self.VIMEO_TYPE:
            return 'https://vimeo.com/%s' % self.code

    @property
    def type_string(self):
        return 'youtube' if self.type == self.YOUTUBE_TYPE else 'vimeo'


class Attachment(models.Model):
    company = models.ForeignKey(
        Company, verbose_name='azienda', related_name='attachments')
    name = models.CharField(verbose_name='nome', max_length=128)
    file = models.FileField(upload_to='agreements/attachment')

    @property
    def extension(self):
        return os.path.splitext(self.file.url)[1][1:]

    @property
    def size(self):
        kb = self.file.size / 1000
        if (kb):
            return '%s %s' % (str(kb), 'kB')
        else:
            return '%s %s' % (self.file.size, 'B')

    class Meta:
        verbose_name = 'allegato'
        verbose_name_plural = 'allegati'

    def __unicode__(self):
        return '%s' % self.name


class Proposal(models.Model):
    date = models.DateTimeField('inserimento', auto_now_add=True)
    firstname = models.CharField('nome', max_length=150)
    lastname = models.CharField('cognome', max_length=150)
    phone = models.CharField('telefono', max_length=20)
    head_office = models.CharField('sede sindacale', max_length=150)
    referer = models.CharField(
        'referente/titolare', max_length=150, blank=True, null=True)
    begin_date = models.CharField('data stipula convenzione', max_length=100)
    end_date = models.CharField(
        'data fine convenzione', max_length=100, blank=True, null=True)
    company_name = models.CharField('nome azienda', max_length=255)
    company_address = models.CharField('indirizzo azienda', max_length=255)
    company_region = models.ForeignKey(
        Region, verbose_name='regione azienda', blank=True, null=True)
    company_region_other = models.CharField(
        'altra regione azienda', max_length=255, blank=True, null=True)
    company_province = models.ForeignKey(
        Province,
        verbose_name='provincia azienda',  # noqa
        blank=True,
        null=True)
    company_province_other = models.CharField(
        'altra provincia azienda', max_length=255, blank=True, null=True)
    company_city = models.ForeignKey(
        City, verbose_name='comune azienda', blank=True, null=True)
    company_city_other = models.CharField(
        'altro comune azienda', max_length=255, blank=True, null=True)
    company_phone = models.CharField('telefono azienda', max_length=50)
    company_web = models.CharField(
        'sito web azienda', max_length=255, blank=True, null=True)
    company_email = models.CharField(
        'e-mail azienda', max_length=255, blank=True, null=True)
    company_category = models.ForeignKey(
        Category,
        verbose_name='categoria azienda',  # noqa
        blank=True,
        null=True)
    company_category_other = models.CharField(
        'altra categoria azienda', max_length=255, blank=True, null=True)
    company_category_other2 = models.CharField(
        'seconda altra categoria azienda', max_length=255, blank=True,
        null=True)
    company_description = models.TextField('descrizione azienda')
    site = models.ForeignKey(Site, verbose_name='sito')

    class Meta:
        verbose_name = "Convenzione suggerita"
        verbose_name_plural = "Convenzioni suggerite"

    def __unicode__(self):
        return self.company_name


class CouponRegistration(models.Model):
    firstname = models.CharField('nome', max_length=128)
    lastname = models.CharField('cognome', max_length=128)
    email = models.EmailField('e-mail', max_length=128)
    phone = models.CharField('telefono', max_length=32)
    category = models.CharField('categoria sindacale', max_length=128)
    cap = models.CharField('cap', max_length=8)
    gdpr = models.BooleanField('accettazione gdpr', default=False)
    gdpr_dt = models.DateTimeField('accettazione gdp data e ora', auto_now_add=True)
    company = models.ForeignKey(Company, verbose_name='azienda', blank=True, null=True)
    site = models.ForeignKey(Site, verbose_name='sito')

    class Meta:
        verbose_name = "registrazione codice sconto"
        verbose_name_plural = "registrazioni codici sconto"

    def __unicode__(self):
        return '%s %s' % (self.lastname, self.firstname)


class SpamIndicator(models.Model):
    name = models.CharField('nome', max_length=50, help_text='puoi testare qui: https://repl.it/@abidibo/Spam-Detection#main.py')
    pattern = models.CharField('pattern', max_length=50, help_text=('Viene utilizzato come regular expression, se il messaggio matcha viene considerato spam'))
    ignore_case = models.BooleanField('ignora maiuscole/minuscole', default=True)
    dot_matches_newline = models.BooleanField('il punto matcha anche newline', default=False)
    active = models.BooleanField('attivo', default=True)

    class Meta:
        verbose_name = "indicatore spam"
        verbose_name_plural = "indicatori spam"

    def __unicode__(self):
        return self.name
