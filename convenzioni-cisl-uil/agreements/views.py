import re
from django.conf import settings
from django.core.mail import send_mail
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone
from django.views.generic import View
from rest_framework import viewsets

from .forms import ContactForm, CouponForm
from .models import Category, Company, Point, Province, Region, CouponRegistration, SpamIndicator
from .serializers import CategorySerializer, CompanySerializer, PointSerializer


class AgreementCouponView(View):
    def get(self, request, pk):
        company = Company.objects.get(pk=pk)
        d = {
            'company': company,
            'form': CouponForm(),
        }
        return render(
            request,
            'agreements/coupon_form.html',
            d
        )

    def post(self, request, pk):
        company = Company.objects.get(pk=pk)
        form = CouponForm(request.POST)
        d = {
            'company': company,
            'form': form
        }
        if form.is_valid():
            reg = CouponRegistration(
                firstname=form.cleaned_data['coupon_firstname'],
                lastname=form.cleaned_data['coupon_lastname'],
                email=form.cleaned_data['coupon_email'],
                phone=form.cleaned_data['coupon_phone'],
                category=form.cleaned_data['coupon_category'],
                cap=form.cleaned_data['coupon_cap'],
                company=company,
                site=request.site,
                gdpr=True
            )
            reg.save()
            mail_subject = 'Inserimento richiesta codice sconto da %s %s su %s' % (reg.firstname, reg.lastname, reg.site.name)  # noqa
            mail_object = 'Controlla i dati in area amministrativa: https://www.%s/admin/agreements/couponregistration/%d' % (request.site.domain, reg.id)
            send_mail(mail_subject, mail_object,
                      settings.SITE['MAIL_NOREPLY'],
                      (settings.EMAIL_RTP, ))  # noqa
            return render(
                request,
                'agreements/coupon_success.html'
            )
        else:
            return render(
                request,
                'agreements/coupon_form.html',
                d
            )


class AgreementsListView(View):
    def get_companies(self, post):

        region_id = post.get('sa_region', None)
        province_id = post.get('sa_province', None)
        category_id = post.get('sa_category', None)
        keyword = post.get('sa_keyword', None)

        region = None
        if region_id:
            region = get_object_or_404(Region, id=region_id)

        province = None
        if province_id:
            province = get_object_or_404(Province, id=province_id)

        category = None
        if category_id:
            category = get_object_or_404(Category, id=category_id)

        companies = Company.objects.filter(
            Q(date_to__isnull=True) | Q(date_to__gte=timezone.now()),
            date_from__lte=timezone.now(),
            sites__in=[self.request.site])

        # province and region
        if province:
            companies = companies.filter(
                Q(visibility_italy=True) | Q(points__province=province)
                | Q(points__visibility_provinces__in=[province]),  # noqa
            )
        elif region:
            companies = companies.filter(
                Q(visibility_italy=True) | Q(points__region=region)
                | Q(points__visibility_provinces__region__in=[region]),  # noqa
            )

        if category:
            companies = companies.filter(categories__in=[category])

        if keyword:
            companies = companies.filter(
                Q(tags__name__icontains=keyword.rstrip()) | Q(name__icontains=keyword.rstrip())
                | Q(text__icontains=keyword.rstrip())  # noqa
            )

        return companies

    def get(self, request, category_slug, *args, **kwargs):

        category = None
        if category_slug:
            request.session['search_agreement'] = {}
            category = get_object_or_404(Category, slug=category_slug)

            companies = Company.objects.filter(
                Q(date_to__isnull=True) | Q(date_to__gte=timezone.now()),
                date_from__lte=timezone.now(),
                sites__in=[self.request.site])

            companies = companies.filter(categories__in=[category])
        else:
            companies = self.get_companies(
                request.session.get('search_agreement', {}))  # noqa

        dict = {
            'companies': companies.distinct().order_by('relevance'),
            'category': category
        }

        return render(request, 'agreements/list.html', dict)

    def post(self, request, *args, **kwargs):

        request.session['search_agreement'] = request.POST

        companies = self.get_companies(request.POST)

        dict = {'companies': companies.distinct().order_by('relevance')}

        return render(request, 'agreements/list.html', dict)


class AgreementDetailView(View):
    def get(self, request, slug, pk, form=None, **kwargs):

        company = get_object_or_404(Company, pk=pk, sites__in=[request.site])

        province_id = request.session.get('search_agreement', {}).get(
            'sa_province', None)  # noqa
        region_id = request.session.get('search_agreement', {}).get(
            'sa_region', None)  # noqa

        h_points = company.points.all()
        o_points = []
        if province_id:
            province = get_object_or_404(Province, pk=province_id)
            h_points = company.points.filter(
                Q(province=province) | Q(visibility_provinces__in=[province]))
            o_points = company.points.exclude(
                Q(province=province) | Q(visibility_provinces__in=[province]))
        elif region_id:
            region = get_object_or_404(Region, pk=region_id)
            h_points = company.points.filter(
                Q(region=region)
                | Q(visibility_provinces__region__in=[region])).distinct()
            o_points = company.points.exclude(
                Q(region=region)
                | Q(visibility_provinces__region__in=[region])).distinct()

        dict = {
            'company': company,
            'h_points': h_points,
            'o_points': o_points,
            'form': ContactForm() if form is None else form,
            'from_form': False if form is None else True
        }

        return render(request, 'agreements/detail.html', dict)

    def post(self, request, slug, pk, *args, **kwargs):
        company = get_object_or_404(Company, pk=pk, sites__in=[request.site])

        form = ContactForm(request.POST)

        if form.is_valid():
            email = request.POST.get('fc_email', None)
            message = request.POST.get('fc_text', None)

            if email and message and not self.is_spam(message):
                mail_subject = '%s - Richiesta informazioni da %s' % (
                    request.site.name, email)  # noqa
                mail_subject_rtp = '%s - %s - Richiesta informazioni da %s' % (
                    request.site.name, company.name, email)  # noqa
                mail_object = message
                send_mail(mail_subject, mail_object,
                          settings.SITE['MAIL_NOREPLY'],
                          (company.email, ))  # noqa
                send_mail(mail_subject_rtp, mail_object,
                          settings.SITE['MAIL_NOREPLY'],
                          (settings.EMAIL_RTP, ))  # noqa

            return redirect('/convenzioni/%s/%s/#ok' % (company.slug,
                                                     str(company.pk)))
        else:
            return self.get(request, slug, pk, form, **kwargs)

    def is_spam(self, message):
        for s in SpamIndicator.objects.filter(active=True):
            flags = []
            if s.ignore_case:
                flags.append(re.IGNORECASE)
            if s.dot_matches_newline:
                flags.append(re.DOTALL)
            if re.search(s.pattern, message, *flags) is not None:
                print('SPAM detected')
                return True

        print('SPAM NOT detected')
        return False

# API
class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to fetch categories
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def get_queryset(self):
        return Category.objects.filter(sites__in=[self.request.site])


class CompanyViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to fetch companies
    """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    def get_queryset(self):
        return Company.objects.filter(
            Q(date_to__isnull=True) | Q(date_to__gte=timezone.now()),
            date_from__lte=timezone.now(),
            show_in_app=True,
            sites__in=[self.request.site])


class PointViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to fetch points
    """
    queryset = Point.objects.all()
    serializer_class = PointSerializer

    def get_queryset(self):
        return Point.objects.filter(
            Q(company__date_to__isnull=True)
            | Q(company__date_to__gte=timezone.now()),
            company__date_from__lte=timezone.now(),
            company__show_in_app=True,
            company__sites__in=[self.request.site
                                ]).order_by('company__relevance')
