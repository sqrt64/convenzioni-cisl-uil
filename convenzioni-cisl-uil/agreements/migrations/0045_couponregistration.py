# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('agreements', '0044_auto_20190321_1256'),
    ]

    operations = [
        migrations.CreateModel(
            name='CouponRegistration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('firstname', models.CharField(max_length=128, verbose_name=b'nome')),
                ('lastname', models.CharField(max_length=128, verbose_name=b'cognome')),
                ('email', models.EmailField(max_length=128, verbose_name=b'e-mail')),
                ('phone', models.CharField(max_length=32, verbose_name=b'telefono')),
                ('category', models.CharField(max_length=128, verbose_name=b'categoria sindacale')),
                ('cap', models.CharField(max_length=8, verbose_name=b'cap')),
                ('gdpr', models.BooleanField(default=False, verbose_name=b'accettazione gdpr')),
                ('gdpr_dt', models.DateTimeField(auto_now_add=True, verbose_name=b'accettazione gdp data e ora')),
                ('site', models.ForeignKey(verbose_name=b'sito', to='sites.Site')),
            ],
            options={
                'verbose_name': 'registrazione codice sconto',
                'verbose_name_plural': 'registrazioni codici sconto',
            },
        ),
    ]
