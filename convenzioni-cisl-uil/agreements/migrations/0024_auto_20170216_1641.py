# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0023_auto_20170216_1633'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2017, 2, 16, 15, 41, 31, 623707, tzinfo=utc), verbose_name=b'da'),
        ),
        migrations.AlterField(
            model_name='company',
            name='proposal',
            field=models.OneToOneField(null=True, blank=True, to='agreements.Proposal', verbose_name=b'suggerimento'),
        ),
    ]
