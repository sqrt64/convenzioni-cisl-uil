# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0010_auto_20161206_1038'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='attachment',
            name='point',
        ),
        migrations.RemoveField(
            model_name='image',
            name='point',
        ),
        migrations.RemoveField(
            model_name='video',
            name='point',
        ),
        migrations.AddField(
            model_name='attachment',
            name='company',
            field=models.ForeignKey(related_name='attachments', default='', verbose_name=b'azienda', to='agreements.Company'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='image',
            name='company',
            field=models.ForeignKey(related_name='images', default='', verbose_name=b'azienda', to='agreements.Company'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='video',
            name='company',
            field=models.ForeignKey(related_name='videos', default='', verbose_name=b'azienda', to='agreements.Company'),
            preserve_default=False,
        ),
    ]
