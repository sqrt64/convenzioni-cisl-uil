# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0037_auto_20180727_1706'),
    ]

    operations = [
        migrations.AddField(
            model_name='point',
            name='textual_address',
            field=ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name=b'indirizzo testuale', blank=True),
        ),
    ]
