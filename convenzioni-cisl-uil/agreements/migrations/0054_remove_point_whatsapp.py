# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0053_auto_20210127_1013'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='point',
            name='whatsapp',
        ),
    ]
