# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0014_auto_20161206_1141'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='visibility_regions',
        ),
        migrations.AddField(
            model_name='point',
            name='visibility_regions',
            field=models.ManyToManyField(help_text=b'se compilato sar\xc3\xa0 visibile nella sua regione e quelle selezionate qui. ', related_name='visibility_points', verbose_name=b'regioni di visibilit\xc3\xa0', to='agreements.Region', blank=True),
        ),
    ]
