# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0032_auto_20180119_1727'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2018, 2, 14, 11, 53, 25, 288496, tzinfo=utc), verbose_name=b'da'),
        ),
    ]
