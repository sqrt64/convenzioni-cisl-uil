# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0016_auto_20161206_1414'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='company',
            options={'ordering': ('name',), 'verbose_name': 'Azienda', 'verbose_name_plural': 'Aziende'},
        ),
        migrations.RemoveField(
            model_name='point',
            name='contact_form',
        ),
        migrations.AddField(
            model_name='company',
            name='contact_form',
            field=models.BooleanField(default=False, verbose_name=b'form di contatto'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='company',
            name='visibility_italy',
            field=models.BooleanField(default=False, verbose_name=b'visibile in tutta Italia'),
        ),
        migrations.AlterField(
            model_name='point',
            name='company',
            field=models.ForeignKey(related_name='points', verbose_name=b'azienda', to='agreements.Company'),
        ),
    ]
