# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='point',
            name='slug',
            field=models.SlugField(default='', verbose_name=b'slug'),
            preserve_default=False,
        ),
    ]
