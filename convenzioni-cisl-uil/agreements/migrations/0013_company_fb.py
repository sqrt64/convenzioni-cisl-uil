# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0012_auto_20161206_1135'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='fb',
            field=models.URLField(null=True, verbose_name=b'pagina facebook', blank=True),
        ),
    ]
