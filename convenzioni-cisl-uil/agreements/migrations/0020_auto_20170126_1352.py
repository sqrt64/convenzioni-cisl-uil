# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('agreements', '0019_auto_20161222_1428'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='sites',
            field=models.ManyToManyField(to='sites.Site', verbose_name=b'siti'),
        ),
        migrations.AddField(
            model_name='video',
            name='sites',
            field=models.ManyToManyField(to='sites.Site', verbose_name=b'siti'),
        ),
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2017, 1, 26, 12, 52, 49, 914014, tzinfo=utc), verbose_name=b'da'),
        ),
        migrations.AlterField(
            model_name='video',
            name='type',
            field=models.IntegerField(verbose_name=b'piattaforma', choices=[(1, b'youtube'), (2, b'vimeo')]),
        ),
    ]
