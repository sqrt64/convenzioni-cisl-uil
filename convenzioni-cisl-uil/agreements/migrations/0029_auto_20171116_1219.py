# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0028_auto_20170526_0845'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2017, 11, 16, 11, 19, 4, 715761, tzinfo=utc), verbose_name=b'da'),
        ),
        migrations.AlterField(
            model_name='point',
            name='visibility_provinces',
            field=models.ManyToManyField(help_text=b'se compilato sar\xc3\xa0 visibile nella sua provincia e quelle selezionate qui. ', related_name='visibility_points', verbose_name='province di visibilit\xe0', to='agreements.Province', blank=True),
        ),
    ]
