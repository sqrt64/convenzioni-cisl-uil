# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0004_auto_20161124_0917'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ('name',), 'verbose_name': 'Categoria', 'verbose_name_plural': 'Categorie'},
        ),
        migrations.AlterModelOptions(
            name='city',
            options={'ordering': ('name',), 'verbose_name': 'Comune', 'verbose_name_plural': 'Comuni'},
        ),
        migrations.AlterModelOptions(
            name='province',
            options={'ordering': ('name',), 'verbose_name': 'Provincia', 'verbose_name_plural': 'Province'},
        ),
        migrations.AlterModelOptions(
            name='region',
            options={'ordering': ('name',), 'verbose_name': 'Regione', 'verbose_name_plural': 'Regioni'},
        ),
        migrations.RemoveField(
            model_name='point',
            name='visibility_italy',
        ),
        migrations.AlterField(
            model_name='point',
            name='visibility_region',
            field=models.ForeignKey(related_name='visibility_points', blank=True, to='agreements.Region', help_text=b'se vuoto sar\xc3\xa0 visibile in tutta Italia', null=True, verbose_name=b'visibilt\xc3\xa0 limitata a regione'),
        ),
    ]
