# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0054_remove_point_whatsapp'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='mobile',
            field=models.CharField(max_length=50, null=True, verbose_name=b'cellulare', blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='whatsapp',
            field=models.CharField(max_length=50, null=True, verbose_name=b'numero whatsapp', blank=True),
        ),
        migrations.AddField(
            model_name='point',
            name='whatsapp',
            field=models.CharField(max_length=50, null=True, verbose_name=b'numero whatsapp', blank=True),
        ),
    ]
