# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0020_auto_20170126_1352'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='slideshow',
            field=models.BooleanField(default=False, verbose_name=b'attiva slideshow'),
        ),
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2017, 2, 2, 10, 45, 17, 935114, tzinfo=utc), verbose_name=b'da'),
        ),
    ]
