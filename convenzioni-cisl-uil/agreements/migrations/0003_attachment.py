# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0002_point_slug'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name=b'nome')),
                ('file', models.FileField(upload_to=b'agreements/attachment')),
                ('point', models.ForeignKey(related_name='attachments', verbose_name=b'punto convenzionato', to='agreements.Point')),
            ],
            options={
                'verbose_name': 'allegato',
                'verbose_name_plural': 'allegati',
            },
        ),
    ]
