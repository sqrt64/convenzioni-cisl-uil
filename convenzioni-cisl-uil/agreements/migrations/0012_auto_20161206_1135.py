# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0011_auto_20161206_1053'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='email',
            field=models.EmailField(max_length=254, null=True, verbose_name=b'email', blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='phone',
            field=models.CharField(max_length=50, null=True, verbose_name=b'telefono', blank=True),
        ),
        migrations.AddField(
            model_name='company',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(default='', verbose_name=b'testo'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='company',
            name='web',
            field=models.URLField(null=True, verbose_name=b'web', blank=True),
        ),
        migrations.AlterField(
            model_name='point',
            name='company',
            field=models.ForeignKey(default='', verbose_name=b'azienda', to='agreements.Company'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='point',
            name='lat',
            field=models.CharField(max_length=50, null=True, verbose_name=b'latitudine', blank=True),
        ),
        migrations.AlterField(
            model_name='point',
            name='lng',
            field=models.CharField(max_length=50, null=True, verbose_name=b'longitudine', blank=True),
        ),
        migrations.AlterField(
            model_name='point',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name=b'testo', blank=True),
        ),
    ]
