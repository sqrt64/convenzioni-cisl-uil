# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0039_remove_point_textual_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='textual_address',
            field=ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name=b'indirizzo testuale', blank=True),
        ),
    ]
