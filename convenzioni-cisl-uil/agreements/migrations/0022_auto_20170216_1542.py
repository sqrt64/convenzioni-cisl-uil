# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0021_auto_20170202_1145'),
    ]

    operations = [
        migrations.CreateModel(
            name='Proposal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name=b'inserimento')),
                ('firstname', models.CharField(max_length=150, verbose_name=b'nome')),
                ('lastname', models.CharField(max_length=150, verbose_name=b'cognome')),
                ('phone', models.CharField(max_length=20, verbose_name=b'telefono')),
                ('head_office', models.CharField(max_length=150, verbose_name=b'sede sindacale')),
                ('referer', models.CharField(max_length=150, null=True, verbose_name=b'referente/titolare', blank=True)),
                ('begin_date', models.CharField(max_length=100, verbose_name=b'data stipula convenzione')),
                ('end_date', models.CharField(max_length=100, null=True, verbose_name=b'data fine convenzione', blank=True)),
                ('company_name', models.CharField(max_length=255, verbose_name=b'nome azienda')),
                ('company_address', models.CharField(max_length=255, verbose_name=b'indirizzo azienda')),
                ('company_region_other', models.CharField(max_length=255, null=True, verbose_name=b'altra regione azienda', blank=True)),
                ('company_province_other', models.CharField(max_length=255, null=True, verbose_name=b'altra provincia azienda', blank=True)),
                ('company_city_other', models.CharField(max_length=255, null=True, verbose_name=b'altro comune azienda', blank=True)),
                ('company_phone', models.CharField(max_length=50, verbose_name=b'telefono azienda')),
                ('company_web', models.CharField(max_length=255, null=True, verbose_name=b'sito web azienda', blank=True)),
                ('company_email', models.CharField(max_length=255, null=True, verbose_name=b'e-mail azienda', blank=True)),
                ('company_category_other', models.CharField(max_length=255, null=True, verbose_name=b'altra categoria azienda', blank=True)),
                ('company_description', models.TextField(verbose_name=b'descrizione azienda')),
                ('company_category', models.ForeignKey(verbose_name=b'comune azienda', blank=True, to='agreements.Category', null=True)),
                ('company_city', models.ForeignKey(verbose_name=b'comune azienda', blank=True, to='agreements.City', null=True)),
                ('company_province', models.ForeignKey(verbose_name=b'provincia azienda', blank=True, to='agreements.Province', null=True)),
                ('company_region', models.ForeignKey(verbose_name=b'regione azienda', blank=True, to='agreements.Region', null=True)),
            ],
            options={
                'verbose_name': 'Convenzione suggerita',
                'verbose_name_plural': 'Convenzioni suggerite',
            },
        ),
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2017, 2, 16, 14, 42, 56, 345213, tzinfo=utc), verbose_name=b'da'),
        ),
        migrations.AlterField(
            model_name='company',
            name='slideshow',
            field=models.BooleanField(default=False, help_text=b'dimensioni ottimali immagini: 650x325', verbose_name=b'attiva slideshow'),
        ),
    ]
