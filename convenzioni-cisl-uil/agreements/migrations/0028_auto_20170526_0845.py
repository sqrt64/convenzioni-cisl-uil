# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0027_auto_20170329_0951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2017, 5, 26, 6, 45, 12, 849044, tzinfo=utc), verbose_name=b'da'),
        ),
    ]
