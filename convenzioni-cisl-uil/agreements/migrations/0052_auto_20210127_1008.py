# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0051_auto_20201223_0938'),
    ]

    operations = [
        migrations.AddField(
            model_name='proposal',
            name='company_category_other2',
            field=models.CharField(max_length=255, null=True, verbose_name=b'seconda altra categoria azienda', blank=True),
        ),
        migrations.AlterField(
            model_name='spamindicator',
            name='name',
            field=models.CharField(help_text=b'puoi testare qui: https://repl.it/@abidibo/Spam-Detection#main.py', max_length=50, verbose_name=b'nome'),
        ),
    ]
