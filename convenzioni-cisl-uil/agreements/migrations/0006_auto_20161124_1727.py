# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0005_auto_20161124_1709'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='point',
            name='visibility_region',
        ),
        migrations.AddField(
            model_name='point',
            name='visibility_regions',
            field=models.ManyToManyField(related_name='visibility_points', to='agreements.Region', blank=True, help_text=b'se vuoto sar\xc3\xa0 visibile in tutta Italia, se compilato sar\xc3\xa0 visibile solamente nella sua regione e quell selezionate qui', null=True, verbose_name=b'visibilt\xc3\xa0 limitata'),
        ),
    ]
