# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0049_spamindicator_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='spamindicator',
            name='dot_matches_newline',
            field=models.BooleanField(default=False, verbose_name=b'il punto matcha anche newline'),
        ),
        migrations.AddField(
            model_name='spamindicator',
            name='ignore_case',
            field=models.BooleanField(default=True, verbose_name=b'ignora maiuscole/minuscole'),
        ),
    ]
