# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0048_spamindicator'),
    ]

    operations = [
        migrations.AddField(
            model_name='spamindicator',
            name='active',
            field=models.BooleanField(default=True, verbose_name=b'attivo'),
        ),
    ]
