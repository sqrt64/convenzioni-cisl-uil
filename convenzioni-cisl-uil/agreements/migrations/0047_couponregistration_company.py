# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0046_company_coupon_btn_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='couponregistration',
            name='company',
            field=models.ForeignKey(verbose_name=b'azienda', blank=True, to='agreements.Company', null=True),
        ),
    ]
