# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0024_auto_20170216_1641'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='use_in_card',
            field=models.BooleanField(default=False, verbose_name=b'Usa nella scheda'),
        ),
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2017, 2, 23, 6, 55, 3, 391511, tzinfo=utc), verbose_name=b'da'),
        ),
        migrations.AlterField(
            model_name='company',
            name='proposal',
            field=models.OneToOneField(related_name='company', null=True, blank=True, to='agreements.Proposal', verbose_name=b'suggerimento'),
        ),
    ]
