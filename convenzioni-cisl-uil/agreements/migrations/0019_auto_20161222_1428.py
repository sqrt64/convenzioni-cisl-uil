# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0018_auto_20161214_1740'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='phone2',
            field=models.CharField(max_length=50, null=True, verbose_name=b'altro telefono', blank=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2016, 12, 22, 13, 28, 14, 645501, tzinfo=utc), verbose_name=b'da'),
        ),
    ]
