# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0052_auto_20210127_1008'),
    ]

    operations = [
        migrations.AddField(
            model_name='point',
            name='mobile',
            field=models.CharField(max_length=50, null=True, verbose_name=b'cellulare', blank=True),
        ),
        migrations.AddField(
            model_name='point',
            name='whatsapp',
            field=models.BooleanField(default=False, verbose_name=b'contatto whatsapp'),
        ),
    ]
