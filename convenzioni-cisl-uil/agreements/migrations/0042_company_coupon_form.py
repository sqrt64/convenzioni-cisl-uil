# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0041_company_show_in_app'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='coupon_form',
            field=models.BooleanField(default=False, verbose_name=b'form di richiesta codici sconto'),
        ),
    ]
