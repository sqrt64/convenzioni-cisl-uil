# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0043_company_coupon_form_text'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='coupon_form_text',
            field=models.TextField(default=b"Compila il modulo in ogni sua parte per richiedere il tuo codice sconto: te lo invieremo al pi\xc3\xb9 presto all'email da te indicata o tramite messaggio whatsapp. Alla stessa email invieremo, mensilmente, notizie su nuove convenzioni e promozioni speciali riservate agli iscritti.", null=True, verbose_name=b'testo mostrato prima del form', blank=True),
        ),
    ]
