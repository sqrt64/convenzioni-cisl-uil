# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import smart_selects.db_fields
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0013_company_fb'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='color',
        ),
        migrations.AddField(
            model_name='company',
            name='summary',
            field=ckeditor_uploader.fields.RichTextUploadingField(default='', verbose_name=b'descrizione breve'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='point',
            name='city',
            field=smart_selects.db_fields.ChainedForeignKey(chained_model_field=b'province', to='agreements.City', chained_field=b'province', auto_choose=True, verbose_name=b'citt\xc3\xa0'),
        ),
        migrations.AlterField(
            model_name='point',
            name='province',
            field=smart_selects.db_fields.ChainedForeignKey(chained_model_field=b'region', to='agreements.Province', chained_field=b'region', auto_choose=True, verbose_name=b'provincia'),
        ),
    ]
