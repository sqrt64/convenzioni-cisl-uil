# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0038_point_textual_address'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='point',
            name='textual_address',
        ),
    ]
