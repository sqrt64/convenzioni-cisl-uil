# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0003_attachment'),
    ]

    operations = [
        migrations.AddField(
            model_name='province',
            name='name',
            field=models.CharField(default='', max_length=200, verbose_name=b'nome'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='point',
            name='visibility_italy',
            field=models.BooleanField(help_text=b'mutuo esclusivo con visibilit\xc3\xa0 regione', verbose_name=b'visibile tutta Italia'),
        ),
        migrations.AlterField(
            model_name='point',
            name='visibility_region',
            field=models.ForeignKey(related_name='visibility_points', verbose_name=b'visibilt\xc3\xa0 regione', blank=True, to='agreements.Region', null=True),
        ),
    ]
