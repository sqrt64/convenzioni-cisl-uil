# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0030_auto_20171116_1227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2017, 11, 16, 11, 27, 35, 991264, tzinfo=utc), verbose_name=b'da'),
        ),
    ]
