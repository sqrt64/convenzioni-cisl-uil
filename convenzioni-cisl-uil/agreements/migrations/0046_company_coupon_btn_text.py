# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0045_couponregistration'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='coupon_btn_text',
            field=ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name=b'testo sopra bottone richiedi sconto', blank=True),
        ),
    ]
