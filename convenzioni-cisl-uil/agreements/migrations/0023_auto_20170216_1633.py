# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0022_auto_20170216_1542'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='proposal',
            field=models.ForeignKey(blank=True, to='agreements.Proposal', null=True),
        ),
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2017, 2, 16, 15, 33, 54, 294303, tzinfo=utc), verbose_name=b'da'),
        ),
        migrations.AlterField(
            model_name='proposal',
            name='company_category',
            field=models.ForeignKey(verbose_name=b'categoria azienda', blank=True, to='agreements.Category', null=True),
        ),
    ]
