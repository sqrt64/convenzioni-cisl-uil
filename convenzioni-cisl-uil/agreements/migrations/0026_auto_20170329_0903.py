# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('agreements', '0025_auto_20170223_0755'),
    ]

    operations = [
        migrations.AddField(
            model_name='proposal',
            name='site',
            field=models.ForeignKey(default=1, verbose_name=b'sito', to='sites.Site'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2017, 3, 29, 7, 2, 53, 995403, tzinfo=utc), verbose_name=b'da'),
        ),
    ]
