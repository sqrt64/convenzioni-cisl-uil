# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import smart_selects.db_fields
import colorful.fields
import taggit.managers
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'nome')),
                ('slug', models.SlugField(verbose_name=b'slug')),
                ('color', colorful.fields.RGBColorField(verbose_name=b'colore')),
                ('old_id', models.IntegerField(null=True, verbose_name=b'vecchio ID', blank=True)),
                ('sites', models.ManyToManyField(to='sites.Site', verbose_name=b'siti')),
            ],
            options={
                'verbose_name': 'Categoria',
                'verbose_name_plural': 'Categorie',
            },
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'nome')),
            ],
            options={
                'verbose_name': 'Comune',
                'verbose_name_plural': 'Comuni',
            },
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'titolo')),
                ('file', models.ImageField(upload_to=b'agreements/point/img/')),
                ('date_creation', models.DateTimeField(auto_now_add=True, verbose_name=b'inserimento')),
                ('last_edit', models.DateTimeField(auto_now=True, verbose_name=b'ultima modifica')),
            ],
            options={
                'verbose_name': 'Immagine',
                'verbose_name_plural': 'Immagini',
            },
        ),
        migrations.CreateModel(
            name='Point',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'nome')),
                ('address', models.CharField(max_length=180, verbose_name=b'indirizzo')),
                ('cap', models.CharField(max_length=5, verbose_name=b'cap')),
                ('lat', models.CharField(max_length=50, verbose_name=b'latitudine')),
                ('lng', models.CharField(max_length=50, verbose_name=b'longitudine')),
                ('phone', models.CharField(max_length=50, null=True, verbose_name=b'telefono', blank=True)),
                ('email', models.EmailField(max_length=254, null=True, verbose_name=b'email', blank=True)),
                ('web', models.URLField(null=True, verbose_name=b'web', blank=True)),
                ('date_from', models.DateField(verbose_name=b'da')),
                ('date_to', models.DateField(null=True, verbose_name=b'a', blank=True)),
                ('relevance', models.IntegerField(verbose_name=b'priorit\xc3\xa0')),
                ('text', ckeditor_uploader.fields.RichTextUploadingField(verbose_name=b'testo')),
                ('visibility_italy', models.BooleanField(verbose_name=b'visibile tutta Italia')),
                ('contact_form', models.BooleanField(verbose_name=b'form di contatto')),
                ('categories', models.ManyToManyField(related_name='points', verbose_name=b'categorie', to='agreements.Category')),
                ('city', smart_selects.db_fields.ChainedForeignKey(chained_model_field=b'province', chained_field=b'province', auto_choose=True, to='agreements.City')),
            ],
            options={
                'verbose_name': 'Punto convenzionato',
                'verbose_name_plural': 'Punti convenzionati',
            },
        ),
        migrations.CreateModel(
            name='Province',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('abbreviation', models.CharField(max_length=2, verbose_name=b'sigla')),
            ],
            options={
                'verbose_name': 'Provincia',
                'verbose_name_plural': 'Province',
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'nome')),
            ],
            options={
                'verbose_name': 'Regione',
                'verbose_name_plural': 'Regioni',
            },
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'titolo')),
                ('type', models.IntegerField(verbose_name=b'type', choices=[(1, b'youtube'), (2, b'vimeo')])),
                ('code', models.CharField(max_length=64, verbose_name=b'codice')),
                ('width', models.IntegerField(verbose_name=b'larghezza')),
                ('height', models.IntegerField(verbose_name=b'altezza')),
                ('thumb', models.ImageField(help_text=b'se non inserita viene utilizzata una thumb fornita dalla piattaforma', null=True, upload_to=b'agreements/point/video', blank=True)),
                ('date_creation', models.DateTimeField(auto_now_add=True, verbose_name=b'inserimento')),
                ('last_edit', models.DateTimeField(auto_now=True, verbose_name=b'ultima modifica')),
                ('point', models.ForeignKey(related_name='videos', verbose_name=b'punto convenzionato', to='agreements.Point')),
            ],
            options={
                'verbose_name': 'Video',
                'verbose_name_plural': 'Video',
            },
        ),
        migrations.AddField(
            model_name='province',
            name='region',
            field=models.ForeignKey(related_name='provinces', verbose_name=b'regione', to='agreements.Region'),
        ),
        migrations.AddField(
            model_name='point',
            name='province',
            field=smart_selects.db_fields.ChainedForeignKey(chained_model_field=b'region', chained_field=b'region', auto_choose=True, to='agreements.Province'),
        ),
        migrations.AddField(
            model_name='point',
            name='region',
            field=models.ForeignKey(related_name='points', verbose_name=b'regione', to='agreements.Region'),
        ),
        migrations.AddField(
            model_name='point',
            name='sites',
            field=models.ManyToManyField(to='sites.Site', verbose_name=b'siti'),
        ),
        migrations.AddField(
            model_name='point',
            name='tags',
            field=taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', help_text='A comma-separated list of tags.', verbose_name='Tags'),
        ),
        migrations.AddField(
            model_name='point',
            name='visibility_region',
            field=models.ForeignKey(related_name='visibility_points', verbose_name=b'visibilt\xc3\xa0 regione', to='agreements.Region'),
        ),
        migrations.AddField(
            model_name='image',
            name='point',
            field=models.ForeignKey(related_name='images', verbose_name=b'punto convenzionato', to='agreements.Point'),
        ),
        migrations.AddField(
            model_name='city',
            name='province',
            field=models.ForeignKey(related_name='cities', verbose_name=b'provincia', to='agreements.Province'),
        ),
    ]
