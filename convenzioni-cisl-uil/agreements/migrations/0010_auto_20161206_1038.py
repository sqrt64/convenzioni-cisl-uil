# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('sites', '0001_initial'),
        ('agreements', '0009_auto_20161206_1034'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'nome')),
                ('slug', models.SlugField(verbose_name=b'slug')),
                ('date_from', models.DateField(verbose_name=b'da')),
                ('date_to', models.DateField(null=True, verbose_name=b'a', blank=True)),
                ('relevance', models.IntegerField(verbose_name=b'priorit\xc3\xa0')),
                ('visibility_italy', models.BooleanField(default=True, verbose_name=b'visibile in tutta Italia')),
                ('categories', models.ManyToManyField(related_name='points', verbose_name=b'categorie', to='agreements.Category')),
                ('sites', models.ManyToManyField(to='sites.Site', verbose_name=b'siti')),
                ('tags', taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', help_text='A comma-separated list of tags.', verbose_name='Tags')),
                ('visibility_regions', models.ManyToManyField(related_name='visibility_points', to='agreements.Region', blank=True, help_text=b'se compilato sar\xc3\xa0 visibile nella sua regione e quelle selezionate qui. ', null=True, verbose_name=b'regioni di visibilit\xc3\xa0')),
            ],
            options={
                'verbose_name': 'Azienda',
                'verbose_name_plural': 'Aziende',
            },
        ),
        migrations.AddField(
            model_name='point',
            name='company',
            field=models.ForeignKey(verbose_name=b'azienda', blank=True, to='agreements.Company', null=True),
        ),
    ]
