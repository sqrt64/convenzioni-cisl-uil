# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0040_company_textual_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='show_in_app',
            field=models.BooleanField(default=True, verbose_name=b"mostra nell'app"),
        ),
    ]
