# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0031_auto_20171116_1227'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2018, 1, 19, 16, 27, 23, 954789, tzinfo=utc), verbose_name=b'da'),
        ),
    ]
