# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0050_auto_20201223_0936'),
    ]

    operations = [
        migrations.AddField(
            model_name='spamindicator',
            name='name',
            field=models.CharField(default='', max_length=50, verbose_name=b'nome'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='spamindicator',
            name='pattern',
            field=models.CharField(help_text=b'Viene utilizzato come regular expression, se il messaggio matcha viene considerato spam', max_length=50, verbose_name=b'pattern'),
        ),
    ]
