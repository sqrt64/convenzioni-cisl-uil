# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0007_auto_20161124_1730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='point',
            name='visibility_italy',
            field=models.BooleanField(default=True, help_text=b'visibile quando non viene ricercato per regione o provincia', verbose_name=b'visibile in tutta Italia'),
        ),
        migrations.AlterField(
            model_name='point',
            name='visibility_regions',
            field=models.ManyToManyField(related_name='visibility_points', to='agreements.Region', blank=True, help_text=b'se compilato sar\xc3\xa0 visibile nella sua regione e quelle selezionate qui. ', null=True, verbose_name=b'regioni di visibilit\xc3\xa0'),
        ),
    ]
