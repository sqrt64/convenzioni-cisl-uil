# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0008_auto_20161128_0932'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='point',
            name='categories',
        ),
        migrations.RemoveField(
            model_name='point',
            name='date_from',
        ),
        migrations.RemoveField(
            model_name='point',
            name='date_to',
        ),
        migrations.RemoveField(
            model_name='point',
            name='name',
        ),
        migrations.RemoveField(
            model_name='point',
            name='relevance',
        ),
        migrations.RemoveField(
            model_name='point',
            name='sites',
        ),
        migrations.RemoveField(
            model_name='point',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='point',
            name='tags',
        ),
        migrations.RemoveField(
            model_name='point',
            name='visibility_italy',
        ),
        migrations.RemoveField(
            model_name='point',
            name='visibility_regions',
        ),
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(unique=True, verbose_name=b'slug'),
        ),
    ]
