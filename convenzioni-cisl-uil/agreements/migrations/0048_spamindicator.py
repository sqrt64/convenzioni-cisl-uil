# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0047_couponregistration_company'),
    ]

    operations = [
        migrations.CreateModel(
            name='SpamIndicator',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pattern', models.CharField(help_text=b'Vien utilizzato come regular expression, se il messaggio matcha viene considerato spam', max_length=50, verbose_name=b'pattern')),
            ],
            options={
                'verbose_name': 'indicatore spam',
                'verbose_name_plural': 'indicatori spam',
            },
        ),
    ]
