# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0034_auto_20180726_1658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2018, 7, 27, 13, 30, 37, 134216, tzinfo=utc), verbose_name=b'da'),
        ),
    ]
