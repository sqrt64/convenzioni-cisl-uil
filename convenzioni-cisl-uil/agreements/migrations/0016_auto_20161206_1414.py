# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreements', '0015_auto_20161206_1145'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='point',
            name='visibility_regions',
        ),
        migrations.AddField(
            model_name='point',
            name='visibility_provinces',
            field=models.ManyToManyField(help_text=b'se compilato sar\xc3\xa0 visibile nella sua provincia e quelle selezionate qui. ', related_name='visibility_points', verbose_name=b'province di visibilit\xc3\xa0', to='agreements.Province', blank=True),
        ),
    ]
