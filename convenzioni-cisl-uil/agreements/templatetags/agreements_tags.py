from django import template
from django.contrib.sites.shortcuts import get_current_site

from ..models import Region, Province, Category

register = template.Library()


@register.inclusion_tag('agreements/search_agreement_form.html',
                        takes_context=True)
def search_agreement_form(context, category=None):
    request = context['request']
    site = get_current_site(request)

    if site.id == 3:
        default_region = '13'
    else:
        default_region = ''

    regions = Region.objects.all()
    provinces = Province.objects.all()
    categories = Category.objects.filter(
        sites__in=[request.site]
    )

    region_id = int('0' + request.session.get('search_agreement', {}).get('sa_region', default_region)) # noqa
    province_id = int('0' + request.session.get('search_agreement', {}).get('sa_province', '')) # noqa
    if category:
        category_id = category.id
    else:
        category_id = int('0' + request.session.get('search_agreement', {}).get('sa_category', '')) # noqa

    keyword = request.session.get('search_agreement', {}).get('sa_keyword', '') # noqa

    return {
        'regions': regions,
        'provinces': provinces,
        'categories': categories,
        'region_id': region_id,
        'province_id': province_id,
        'category_id': category_id,
        'keyword': keyword,
        'site_id': site.id,
    }
