from django import forms

from captcha.fields import CaptchaField


class ContactForm(forms.Form):
    fc_email = forms.EmailField()
    fc_text = forms.CharField(widget=forms.Textarea)
    terms = forms.BooleanField(error_messages={
        'required': 'Devi accettare le condizioni sulla privacy per inviare il tuo messaggio'
    })
    captcha = CaptchaField()


class CouponForm(forms.Form):
    coupon_firstname = forms.CharField(required=True)
    coupon_lastname = forms.CharField(required=True)
    coupon_email = forms.EmailField(required=True)
    coupon_phone = forms.CharField(required=True)
    coupon_category = forms.CharField(required=True)
    coupon_cap = forms.CharField(required=True)
    coupon_terms = forms.BooleanField(error_messages={
        'required': 'Devi accettare le condizioni sulla privacy per proseguire'
    }, required=True)
    coupon_captcha = CaptchaField()
