from django.apps import AppConfig

class AgreementsConfig(AppConfig):
    name = 'agreements'
    verbose_name = 'Convenzioni'
