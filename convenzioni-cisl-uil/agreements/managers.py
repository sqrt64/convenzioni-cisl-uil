from django.contrib.sites.shortcuts import get_current_site
from django.db import models


class ImageManager(models.Manager):

    def site(self, **kwargs):
        return self.filter(sites__in=[get_current_site(None)], **kwargs)


class VideoManager(models.Manager):

    def site(self, **kwargs):
        return self.filter(sites__in=[get_current_site(None)], **kwargs)
