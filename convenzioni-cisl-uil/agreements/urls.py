from django.conf.urls import url
from .views import AgreementsListView, AgreementDetailView, AgreementCouponView

urlpatterns = [
    url(r'^(?P<category_slug>[\w-]+)?/?$', AgreementsListView.as_view(), name='agreements-list'), # noqa
    url(r'coupon/(?P<pk>[\d]+)/$', AgreementCouponView.as_view(),
        name='agreements-coupon'),
    url(r'^(?P<slug>[-\w]+)/(?P<pk>[\d]+)/$', AgreementDetailView.as_view(),
        name='agreements-detail'),
]
