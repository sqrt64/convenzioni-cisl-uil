from django.conf.urls import patterns,url
from .views import NewsDetailView, NewsListView, NewsCreateView, NewsDeleteView

urlpatterns = patterns('news.views',
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$', NewsDetailView.as_view(), name='news-detail'),
    url(r'^archivio/$', NewsListView.as_view(), name='news-archive'),
    url(r'^crea/$', NewsCreateView.as_view(), name='news-create'),
    url(r'^elimina/(?P<pk>\d+)$', NewsDeleteView.as_view(), name='news-delete'),
)

