# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0004_news_deadline'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='date_from',
            field=models.DateField(default=django.utils.timezone.now, verbose_name=b'da'),
        ),
        migrations.AddField(
            model_name='news',
            name='date_to',
            field=models.DateField(null=True, verbose_name=b'a', blank=True),
        ),
    ]
