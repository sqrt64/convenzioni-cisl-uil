# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0006_news_evidence'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='news',
            name='deadline',
        ),
    ]
