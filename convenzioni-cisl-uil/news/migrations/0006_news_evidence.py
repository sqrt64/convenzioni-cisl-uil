# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0005_auto_20180726_1658'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='evidence',
            field=models.BooleanField(default=False, verbose_name=b'In evidenza'),
        ),
    ]
