# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import colorful.fields
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name=b'nome')),
                ('file', models.FileField(upload_to=b'news/attachment')),
            ],
            options={
                'verbose_name': 'allegato',
                'verbose_name_plural': 'allegati',
            },
        ),
        migrations.CreateModel(
            name='Label',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name=b'nome')),
                ('icon', models.CharField(help_text=b'Utilizzare classi FontAwesome es. fa-exclamation-triangle (http://fortawesome.github.io/Font-Awesome/icons/)', max_length=32, verbose_name=b'icona')),
                ('color', colorful.fields.RGBColorField(verbose_name=b'colore')),
            ],
            options={
                'verbose_name': 'etichetta',
                'verbose_name_plural': 'etichette',
            },
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('insertion_date', models.DateTimeField(auto_now_add=True, verbose_name=b'inserimento')),
                ('last_edit_date', models.DateTimeField(auto_now=True, verbose_name=b'ultima modifica')),
                ('date', models.DateField(null=True, verbose_name=b'data', blank=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'titolo')),
                ('slug', models.SlugField(max_length=255)),
                ('text', ckeditor_uploader.fields.RichTextUploadingField(verbose_name=b'testo')),
                ('featured', models.BooleanField(verbose_name=b'featured')),
                ('show_date', models.BooleanField(verbose_name=b'mostra data')),
                ('published', models.BooleanField(verbose_name=b'pubblicata')),
                ('label', models.ForeignKey(verbose_name=b'etichetta', blank=True, to='news.Label', null=True)),
            ],
            options={
                'verbose_name': 'news',
                'verbose_name_plural': 'news',
            },
        ),
        migrations.AddField(
            model_name='attachment',
            name='news',
            field=models.ForeignKey(related_name='attachments', verbose_name=b'news', to='news.News'),
        ),
    ]
