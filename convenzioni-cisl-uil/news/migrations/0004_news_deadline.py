# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0003_news_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='deadline',
            field=models.DateField(help_text=b'Se vuota la news non scade. Controlla solamente la visibilit\xc3\xa0 in home page', null=True, verbose_name=b'scadenza', blank=True),
        ),
    ]
