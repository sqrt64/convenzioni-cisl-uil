from django.forms import ModelForm
from django.forms.models import inlineformset_factory

from .models import News, Attachment


class NewsForm(ModelForm):
    class Meta:
        model = News
        fields = ['date', 'title', 'text', 'label', 'featured', 'published',]

class AttachmentForm(ModelForm):
    class Meta:
        model = Attachment
        fields = '__all__'

AttachmentFormSet = inlineformset_factory(News, Attachment, form=AttachmentForm, extra=3)
