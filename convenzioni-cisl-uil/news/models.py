# -*- coding: utf-8 -*-
import os

from django.db import models
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.utils import timezone

from ckeditor_uploader.fields import RichTextUploadingField
from colorful.fields import RGBColorField

from .managers import NewsManager


class Label(models.Model):
    name = models.CharField(verbose_name='nome', max_length=64)
    icon = models.CharField(
        verbose_name='icona',
        max_length=32,
        help_text='Utilizzare classi FontAwesome es. fa-exclamation-triangle (http://fortawesome.github.io/Font-Awesome/icons/)' # noqa
    )
    color = RGBColorField(verbose_name='colore')

    class Meta:
        verbose_name = 'etichetta'
        verbose_name_plural = 'etichette'

    def __unicode__(self):
        return '%s' % self.name


class News(models.Model):
    """ News model
    """
    insertion_date = models.DateTimeField('inserimento', auto_now_add=True)
    last_edit_date = models.DateTimeField('ultima modifica', auto_now=True)
    date = models.DateField('data', null=True, blank=True)
    date_from = models.DateField('da', default=timezone.now)
    date_to = models.DateField('a', blank=True, null=True)
    evidence = models.BooleanField('In evidenza', default=False)
    title = models.CharField(verbose_name='titolo', max_length=255)
    slug = models.SlugField(max_length=255)
    image = models.ImageField('immagine', upload_to='news/img')
    text = RichTextUploadingField(verbose_name='testo')
    label = models.ForeignKey(Label, verbose_name='etichetta', null=True,
                              blank=True)
    featured = models.BooleanField('featured')
    show_date = models.BooleanField('mostra data')
    published = models.BooleanField('pubblicata')
    sites = models.ManyToManyField(Site, verbose_name='siti')

    objects = NewsManager()

    class Meta:
        verbose_name = 'news'
        verbose_name_plural = 'news'

    def __unicode__(self):
        return '%s' % self.title

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('news-detail',
                       args=[
                           str(self.insertion_date.year),
                           self.insertion_date.strftime("%m"),
                           self.insertion_date.strftime("%d"), self.slug])

    def get_full_absolute_url(self):
        current_site = get_current_site(None)
        return ''.join([
            'http://',
            current_site.domain,
            self.get_absolute_url()
        ])

    def get_full_img_url(self):
        current_site = get_current_site(None)
        if self.image:
            return ''.join([
                'http://',
                current_site.domain,
                self.image.url
            ])
        return ''


class Attachment(models.Model):
    """ Attachment model
    """
    news = models.ForeignKey(News, verbose_name='news',
                             related_name='attachments')
    name = models.CharField(verbose_name='nome', max_length=128)
    file = models.FileField(upload_to='news/attachment')

    @property
    def extension(self):
        return os.path.splitext(self.file.url)[1][1:]

    @property
    def size(self):
        kb = self.file.size/1000
        if(kb):
            return '%s %s' % (str(kb), 'kB')
        else:
            return '%s %s' % (self.file.size, 'B')

    class Meta:
        verbose_name = 'allegato'
        verbose_name_plural = 'allegati'

    def __unicode__(self):
        return '%s' % self.name
