import logging
import datetime

from django.shortcuts import render
from django.views.generic import View, ListView, DetailView, TemplateView
from django.views.generic.edit import CreateView, DeleteView
from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.db import transaction
from django.template.defaultfilters import slugify
from django.http import Http404

from .models import News
from .forms import NewsForm, AttachmentFormSet

logger = logging.getLogger(__name__)


class NewsDetailView(DetailView):
    """ Single news detail view
    """
    model = News

    def get_queryset(self):
        return News.objects.filter(sites__in=[self.request.site]).published()

    def get_object(self):
        news = News.objects.published(
            slug=self.kwargs['slug'],
            sites__in=[self.request.site]
        )
        #@FIXME needed because probably due to mysql installation month and day lookups do not work!
        if news.count:
            for n in news:
                if n.insertion_date.year == int(self.kwargs['year']) and n.insertion_date.month == int(self.kwargs['month']) and n.insertion_date.day == int(self.kwargs['day']):
                    return n
        else:
            raise Http404

        raise Http404

class NewsListView(ListView):
    """ News archive view
    """
    model = News
    paginate_by = 10

    def get_queryset(self):
        return News.objects.published(sites__in=[self.request.site]).order_by('-insertion_date')  # noqa

class NewsCreateView(CreateView):
    model = News
    form_class = NewsForm

    @method_decorator(permission_required('news.can_add'))
    def dispatch(self, request, *args, **kwargs):
        return super(NewsCreateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super(NewsCreateView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['formset'] = AttachmentFormSet(self.request.POST, self.request.FILES)
        else:
            data['formset'] = AttachmentFormSet()

        return data

    def form_invalid(self, form):
        logger.info('NewsCreateView: form invalid')
        return super(NewsCreateView, self).form_invalid(form)

    @transaction.atomic
    def form_valid(self, form):
        context = self.get_context_data()
        formset = context['formset']
        form.instance.slug = slugify(form.instance.title)
        form.instance.text = '<p>' + form.instance.text.replace('\n','<br />\n') + '</p>'
        self.object = form.save()
        logger.info('NewsCreateView: new object id: %s' % self.object.id)
        if formset.is_valid():
            formset.instance = self.object
            formset.save()
        return render(self.request, 'news/news_create_success.html', {'news': self.object})

class NewsDeleteView(DeleteView):
    model = News
    success_url = '/'

    @method_decorator(permission_required('news.can_delete'))
    def dispatch(self, request, *args, **kwargs):
        return super(NewsDeleteView, self).dispatch(request, *args, **kwargs)
