from django.db import models
from django.db.models import Q
from django.utils import timezone


class NewsManager(models.Manager):
    def published(self, **kwargs):
        return self.filter(published=True, **kwargs)

    def featured(self, **kwargs):
        qs = self.published(featured=True, evidence=False, **kwargs)
        qs = qs.filter(
            Q(date_to__isnull=True) | Q(date_to__gte=timezone.now()),
            date_from__lte=timezone.now())
        return qs

    def evidence_featured(self, **kwargs):
        qs = self.published(featured=True, evidence=True, **kwargs)
        qs = qs.filter(
            Q(date_to__isnull=True) | Q(date_to__gte=timezone.now()),
            date_from__lte=timezone.now())
        return qs
