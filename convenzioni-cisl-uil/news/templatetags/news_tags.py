from django import template
from django.contrib.sites.shortcuts import get_current_site

from ..models import News

register = template.Library()


@register.inclusion_tag('news/showcase.html', takes_context=True)
def news_showcase(context):
    user = context['user']
    current_site = get_current_site(context['request'])
    can_add = True if user and user.has_perm('news.add_news') else False
    return {
        'news': News.objects.featured(
            sites__in=[current_site]
        ).order_by('-date'),
        'can_add': can_add,
        'title': 'News locali',
        'class': 'news',
    }


@register.inclusion_tag('news/evidence.html', takes_context=True)
def news_evidence_showcase(context):
    user = context['user']
    current_site = get_current_site(context['request'])
    can_add = True if user and user.has_perm('news.add_news') else False
    return {
        'news': News.objects.evidence_featured(
            sites__in=[current_site]
        ).order_by('-date'),
        'can_add': can_add,
        'title': 'In evidenza',
        'class': 'evidence',
    }
