# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Label, News, Attachment


class LabelAdmin(admin.ModelAdmin):
    list_display = ['name', 'color', 'icon', ]

admin.site.register(Label, LabelAdmin)


class NewsAttachmentInlineAdmin(admin.TabularInline):
    model = Attachment
    suit_classes = 'suit-tab suit-tab-attachments'


class NewsAdmin(admin.ModelAdmin):
    list_display = ['title', 'date', 'last_edit_date', 'label', 'featured',
                    'published', 'get_sites', ]
    # list_editable = ['published', 'featured', ]
    prepopulated_fields = {'slug': ('title',), }
    inlines = [NewsAttachmentInlineAdmin, ]

    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['date', 'evidence', 'title', 'slug', 'image',
                       'text', 'label', 'show_date', 'featured',
                       'sites', ],
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-visibility',),
            'fields': ['date_from', 'date_to', 'published', ],
        }),
    )

    suit_form_tabs = (
        ('general', 'Generale'),
        ('visibility', 'Visibilità'),
        ('attachments', 'Allegati')
    )

    def get_sites(self, obj):
        return ', '.join([str(s) for s in obj.sites.all()])
    get_sites.short_description = 'siti'


admin.site.register(News, NewsAdmin)
